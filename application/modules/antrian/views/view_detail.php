<div>
    <a href="javascript:;" id="back" class="btn btn-primary btn-md">
        <span class="m-0 font-weight-bold text-default text-center">BACK</span>
    </a>
</div>
<br>
<div class="row">
    <div class="col-md-6">
        <div class="card shadow mb-2 animated--grow-in">
            <div class="card-header py-3">
                <h6 class="m-0 font-weight-bold text-primary text-center">Customer Profile</h6>
            </div>
            <div class="card-body">
                <div class="form-group row">
                    <label class="col-md-4 label-control">No Antrian</label>
                    <div class="col-md-8">
                        <input type="text" class="form-control input-sm" placeholder="No Antrian" name="no_antrian" id="no_antrian" value="<?= $rowdata->kode_antri; ?>" data-error="wajib diisi" required readonly>
                        <div class="help-block with-errors"></div>
                    </div>
                </div>
                <div class="form-group row">
                    <label class="col-md-4 label-control">Nama Pemesan</label>
                    <div class="col-md-8">
                        <input type="text" class="form-control input-sm" placeholder="Nama Pemesan" name="nama_pemesan" id="nama_pemesan" value="<?= $rowdata->nama_pemesan; ?>" data-error="wajib diisi" required readonly>
                        <div class="help-block with-errors"></div>
                    </div>
                </div>
                <div class="form-group row">
                    <label class="col-md-4 label-control">HP</label>
                    <div class="col-md-8">
                        <input type="number" class="form-control input-sm" placeholder="No HP" name="no_hp" id="no_hp" value="<?= $rowdata->no_hp; ?>" data-error="wajib diisi" required readonly>
                        <div class="help-block with-errors"></div>
                    </div>
                </div>
                <div class="form-group row">
                    <label class="col-md-4 label-control">No. Lainnya</label>
                    <div class="col-md-8">
                        <input type="number" class="form-control input-sm" placeholder="No Lainnya" name="no_lainnya" id="no_lainnya" value="<?= $rowdata->no_lainnya; ?>" readonly>
                        <div class="help-block with-errors"></div>
                    </div>
                </div>
                <div class="form-group row">
                    <label class="col-md-4 label-control">Alamat</label>
                    <div class="col-md-8">
                        <textarea class="form-control input-sm" placeholder="Alamat Lengkap" name="alamat" id="alamat" readonly><?= $rowdata->alamat; ?></textarea>
                        <div class="help-block with-errors"></div>
                    </div>
                </div>
                <div class="form-group row">
                    <label class="col-md-4 label-control">Kecamatan</label>
                    <div class="col-md-8">
                        <select class="select2 form-control kecamatan" name="kecamatan" id="kecamatan" style="width:100%" disabled>
                            <option value="">- Pilihan -</option>
                            <?php
                            $n = (isset($rowdata)) ? $rowdata->kode_daerah : '';
                            $q = $this->Data_model->selectData('combospbu', 'kode');
                            foreach ($q as $row) {
                                $kapilih = ($row->kode == $n) ? ' selected=selected' : '';
                                echo '<option data-id="' . $row->kecamatan . '" value="' . $row->kode . '" ' . $kapilih . '>' . $row->nama . '</option>';
                            }
                            ?>
                        </select>
                    </div>
                </div>
                <div class="form-group row">
                    <label class="col-md-4 label-control">SPBU</label>
                    <div class="col-md-8">
                        <select class="select2 form-control kecamatan" name="kecamatan" id="kecamatan" style="width:100%" disabled>
                            <option value="">- Pilihan -</option>
                            <?php
                            $n = (isset($rowdata)) ? $rowdata->kode_spbu : '';
                            $q = $this->Data_model->selectData('m_spbu', 'kode');
                            foreach ($q as $row) {
                                $kapilih = ($row->kode == $n) ? ' selected=selected' : '';
                                echo '<option value="' . $row->kode . '" ' . $kapilih . '>' . $row->nama . '</option>';
                            }
                            ?>
                        </select>
                    </div>
                </div>
                <div class="form-group row">
                    <label class="col-md-4 label-control">Status</label>
                    <div class="col-md-8">
                        <input type="text" class="form-control input-sm" placeholder="status" name="status" id="status" value="<?= $rowdata->status; ?>" data-error="wajib diisi" required readonly>
                        <div class="help-block with-errors"></div>
                    </div>
                </div>
                <div class="form-group row">
                    <label class="col-md-4 label-control">Tanggal Data</label>
                    <div class="col-md-8">
                        <input type="text" class="form-control input-sm" placeholder="Tanggal Data" name="tgl_data" id="tgl_data" value="<?= date('d F Y H:i:s', strtotime($rowdata->tgl_data)); ?>" data-error="wajib diisi" required readonly>
                        <div class="help-block with-errors"></div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div class="col-md-6">
        <div class="card shadow mb-2 animated--grow-in">
            <div class="card-header py-3">
                <h6 class="m-0 font-weight-bold text-primary text-center">Detail Pesanan</h6>
            </div>
            <div class="card-body">
                <div class="row">
                    <div class="col-md-12">
                        <table class="table table-striped" style="width:100%">
                            <thead>
                                <tr>
                                    <th>#</th>
                                    <th>Item</th>
                                    <th>Jumlah</th>
                                    <th>Subtotal</th>
                            </thead>
                            <tbody>
                                <?php $i = 1;
                                foreach ($rowdetail as $row) : ?>
                                    <tr>
                                        <td><?= $i++; ?></td>
                                        <td><?= $row->nama_jenis; ?></td>
                                        <td><?= $row->jumlah; ?></td>
                                        <td><?= "Rp. " . number_format($row->subtotal, 0, ',', '.'); ?></td>
                                    </tr>
                                <?php endforeach; ?>
                            </tbody>
                            <tfoot class="tfoot-b">
                                <tr>
                                    <td>Total</td>
                                    <td></td>
                                    <td></td>
                                    <td><?= "Rp. " . number_format($total[0]->total, 0, ',', '.'); ?></td>
                                </tr>
                            </tfoot>
                        </table>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>

<script>
    $("#back").on("click", function() {
        $("#tabelpegawai").fadeIn('fast');
        $("#containerform").fadeOut();
        $("#contentdetail").html("");
    });
</script>