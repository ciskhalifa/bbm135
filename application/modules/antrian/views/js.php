<!-- Latest compiled and minified JavaScript -->
<script src="<?= base_url() ?>assets/admin/js/formValidation.min.js" type="text/javascript"></script>
<script src="<?= base_url() ?>assets/admin/js/validator.min.js" type="text/javascript"></script>
<script src="<?= base_url() ?>assets/admin/js/jquery.isloading.min.js" type="text/javascript"></script>
<script src="<?= base_url() ?>assets/admin/vendor/datatables/jquery.dataTables.min.js" type="text/javascript"></script>
<script src="<?= base_url() ?>assets/admin/vendor/datatables/dataTables.bootstrap4.min.js"></script>
<script src="<?= base_url() ?>assets/admin/js/dataTables.buttons.min.js" type="text/javascript"></script>
<script src="<?= base_url() ?>assets/admin/js/jszip.min.js" type="text/javascript"></script>
<script src="<?= base_url() ?>assets/admin/js/pdfmake.min.js" type="text/javascript"></script>
<script src="<?= base_url() ?>assets/admin/js/vfs_fonts.js" type="text/javascript"></script>
<script src="<?= base_url() ?>assets/admin/js/buttons.html5.min.js" type="text/javascript"></script>
<script src="<?= base_url() ?>assets/admin/js/buttons.print.min.js" type="text/javascript"></script>
<script src="<?= base_url('assets/admin') ?>/vendor/select2/select2.min.js"></script>
<script src="<?= base_url('assets/admin') ?>/vendor/bootstrap-datetimepicker/moment.min.js"></script>
<script src="<?= base_url('assets/admin') ?>/vendor/bootstrap-datetimepicker/bootstrap-datetimepicker.min.js"></script>

<div id="myConfirm" class="modal animated--grow-in">
    <div class="modal-success">
        <div class="modal-dialog">
            <div class="modal-content">
                <div class="modal-header">
                    <h4 class="modal-title">Konfirmasi</h4>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">×</span></button>
                </div>
                <div class="modal-body">
                    <p>Apakah anda akan menghapus data <span class="lblModal h4"></span> ?</p>
                </div>
                <div class="modal-footer">
                    <input type="hidden" id="cid"><input type="hidden" id="cod"><input type="hidden" id="getto">
                    <button type="button" class="btn btn-primary pull-left" data-dismiss="modal">Batal</button>
                    <button type="button" id="btnYes" class="btn btn-danger">Hapus</button>
                </div>
            </div>
        </div>
    </div>
</div>

<div id="updateStatus" class="modal animated--grow-in">
    <div class="modal-success">
        <div class="modal-dialog">
            <div class="modal-content">
                <div class="modal-header">
                    <h4 class="modal-title">Update Status Antrian</h4>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">×</span></button>
                </div>
                <div class="modal-body">
                    <div class="form-group row">
                        <label class="col-md-4 label-control">No Antrian</label>
                        <div class="col-md-8">
                            <input type="text" class="form-control input-sm no_antrian" placeholder="No Antrian" name="no_antrian" id="antrian" value="" data-error="wajib diisi" required readonly>
                            <div class="help-block with-errors"></div>
                        </div>
                    </div>
                    <div class="form-group row">
                        <label class="col-md-4 label-control">Status</label>
                        <div class="col-md-8">
                            <select class="form-control" name="status" id="status">
                                <option value="">- Pilihan -</option>
                                <option value="ANTRI"> OPEN </option>
                                <option value="ON PROGRESS"> ON PROGRESS </option>
                                <option value="SUCCESS"> CLOSED </option>
                                <option value="CANCEL"> CANCEL </option>
                            </select>
                            <div class="help-block with-errors"></div>
                        </div>
                    </div>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-primary pull-left" data-dismiss="modal">Batal</button>
                    <button type="button" id="btnUpdate" class="btn btn-warning">Update</button>
                </div>
            </div>
        </div>
    </div>
</div>

<script>
    $(function() {
        $('.jenis_pesanan').on('change', function(){
            var jenis = $(this).val();

            if (jenis == 'GAS'){
                $('#list_agent').css('display', 'block');
                $('#list_spbu').css('display', 'none');
            }else if (jenis == 'PDS'){
                $('#list_agent').css('display', 'none');
                $('#list_spbu').css('display', 'block');
            }else{
                $('#list_agent').css('display', 'none');
                $('#list_spbu').css('display', 'none');
            }
        });
        $('#tgl_data').datetimepicker({
            format: 'YYYY-MM-DD',
            icons: {
                time: 'fa fa-clock-o',
                date: 'fa fa-calendar',
                up: 'fa fa-chevron-up',
                down: 'fa fa-chevron-down',
                previous: 'fa fa-chevron-left',
                next: 'fa fa-chevron-right',
                today: 'fa fa-screenshot',
                clear: 'fa fa-trash'
            },
        });
        $("#AntrianTable").dataTable();
        $('#openform').on('click', function() {
            $('#modalForm').modal({
                backdrop: 'static',
                keyboard: false
            });
        });
        $('#modalForm').on('hidden.bs.modal', function() {
            $(this).find('form')[0].reset();
            if ($("#pesanan tbody tr").length > 1) {
                $("#pesanan tbody tr:last-child").remove();
            }
        });
        $(".edit").on('click', function() {
            var status = $(this).data('status');
            console.log(status);
            $("#antrian").val($(this).data('kode'));
            if (status == "ANTRI") {
                $("#updateStatus").modal();
                $('#status').find('[value="ANTRI"]').remove();
            } else if (status == "ON PROGRESS") {
                $("#updateStatus").modal();
                $('#status').find('[value="ANTRI"]').remove();
                $('#status').find('[value="CANCEL"]').remove();
                $('#status').find('[value="ON PROGRESS"]').remove();
            } else if (status == "SUCCESS") {
                alert("Status order sudah selesai");
            } else {
                alert("Status sudah dicancel")
            }
            $('#status').val(status);
        });
        $(".delete").on('click', function() {
            $("#myConfirm").modal();
            $(".lblModal").text($(this).data('kode'));
            $("#cid").val($(this).data('kode'));
            $("#getto").val("<?= base_url('antrian/hapus') ?>");
            $("#cod").val("t_antrian");
        });
        $(".detail").on('click', function() {
            $('#tabelpegawai').hide();
            $('#contentdetail').load('' + 'antrian/view_detail/' + $(this).attr("data-kode"));
            $('#containerdetail').fadeIn('fast');
        });
        $('.kecamatan').select2();
        $('.kecamatan1').select2();
        $("#btnYes").bind("click", function() {
            var link = $("#getto").val();
            $.ajax({
                url: link,
                type: "POST",
                data: "cid=" + $("#cid").val() + "&cod=" + $("#tabel").val(),
                dataType: "html",
                beforeSend: function() {
                    if (link != "#") {}
                },
                success: function(html) {
                    notify("Delete berhasil", "danger")
                    $("#myConfirm").modal("hide")
                    location.reload(true);
                }
            })
        });
        $("#btnUpdate").bind("click", function() {
            var link = "<?= base_url('antrian/updateStatus') ?>";
            $.ajax({
                url: link,
                type: "POST",
                data: "kode=" + $("#antrian").val() + "&status=" + $("#status").val(),
                dataType: "html",
                beforeSend: function() {
                    if (link != "#") {}
                },
                success: function(html) {
                    notify("Update berhasil", "success")
                    $("#updateStatus").modal("hide")
                    location.reload(true);
                }
            })
        });

    });
</script>
<script>
    $(function() {
        var a = $("#pesanan tbody tr").length;

        $(".addpesanan").on("click", function() {
            a++;
            var x = '<td style="text-align: center;">' + (a) + "</td>";
            x += '<td><select class="select2 pilihjenis form-control" id="selectjenis' + (a) + '" name="jenis[]" onchange="loadBarang(' + a + ')"></select></td>';
            x += '<td id="barang' + (a) + '"></td>';
            x += '<td id="jumlah' + (a) + '"></td>';
            $("#pesanan tbody").append('<tr id="row' + (a) + '">' + x + "</tr>");
            selectPesanan(a);
        });

        $(".removepesanan").on("click", function() {
            if ($("#pesanan tbody tr").length > 1) {
                $("#pesanan tbody tr:last-child").remove();
                a--;
            } else {
                alert("Baris pertama isian tidak dapat dihapus")
            }
        });

        var b = $("#pesanangas tbody tr").length;

        $(".addpesanangas").on("click", function() {
            b++;
            var x = '<td style="text-align: center;">' + (b) + "</td>";
            x += '<td><select class="select2 pilihjenisgas form-control" id="selectjenisgas' + (b) + '" name="jenis[]" onchange="loadBarangGas(' + b + ')"></select></td>';
            x += '<td id="baranggas' + (b) + '"></td>';
            x += '<td id="jumlahgas' + (b) + '"></td>';
            $("#pesanangas tbody").append('<tr id="row' + (b) + '">' + x + "</tr>");
            selectPesananGas(b);
        });

        $(".removepesanangas").on("click", function() {
            if ($("#pesanangas tbody tr").length > 1) {
                $("#pesanangas tbody tr:last-child").remove();
                b--;
            } else {
                alert("Baris pertama isian tidak dapat dihapus")
            }
        });
    });

    bukaListJenis();
    bukaListJenisGas();

    function bukaListJenis() {
        if ($("#cid").val() == "") {
            selectPesanan(1);
        } else {
            $.each($(".pilihjenis"), function(j, g) {
                var c = "selectjenis" + (j + 1);
                var b = $("#selectjenis" + (j + 1)).attr("data-default");
                getListCIS("m_jenis", c, "1", "1", $("#selectjenis" + (j + 1)).attr("data-default"), "kode,DISTINCT(tipe) as tipe", "Pilihan", "antrian/getList/");
            });

        }
    }

    function bukaListJenisGas() {
        if ($("#cid").val() == "") {
            selectPesananGas(1);
        } else {
            $.each($(".pilihjenisgas"), function(j, g) {
                var c = "selectjenisgas" + (j + 1);
                var b = $("#selectjenisgas" + (j + 1)).attr("data-default");
                getListCIS("m_jenis", c, "1", "1", $("#selectjenisgas" + (j + 1)).attr("data-default"), "kode,DISTINCT(tipe) as tipe", "Pilihan", "antrian/getList/");
            });

        }
    }

    function selectPesanan(urutan) {
        $.ajax({
            url: "antrian/select_jenis",
            type: "POST",
            success: function(html) {
                json = eval(html);
                $("#selectjenis" + urutan + "").append('<option value="">Pilihan</option>');
                $(json).each(function() {
                    $("#selectjenis" + urutan + " ").append('<option value="' + this.disp + '">' + this.disp + "</option>");
                });
            }
        })
    }

    function selectPesananGas(urutan) {
        $.ajax({
            url: "antrian/select_jenis_gas",
            type: "POST",
            success: function(html) {
                json = eval(html);
                $("#selectjenisgas" + urutan + "").append('<option value="">Pilihan</option>');
                $(json).each(function() {
                    $("#selectjenisgas" + urutan + " ").append('<option value="' + this.disp + '">' + this.disp + "</option>");
                });
            }
        })
    }

    function cekMax(urutan) {
        var dat = [];
        $("#selectjumlah" + urutan).on('change', function() {
            var selected = $(this).children("option:selected").val();
            if (selected !== "") {
                dat.push(selected);
            } else {

            }
        })
        console.log(dat);
    }

    function loadBarangGas(urutan) {

        var tipe = $("#selectjenisgas" + urutan).val();
        var opt = "";
        
        $("#selectjumlahgas" + urutan + " ").remove();
        $("#selectbaranggas" + urutan + " ").remove();
        $("#baranggas" + urutan).append('<select class="select2 pilihbaranggas form-control" id="selectbaranggas' + urutan + '" name="barang[]"></select>');
        $("#jumlahgas" + urutan).append('<input type="number" class="form-control txtjumlah" id= "selectjumlahgas' + urutan + '" name="jumlah[]" placeholder="Jumlah">')
        $.ajax({
            type: 'GET',
            url: "<?= base_url('antrian/getBarang'); ?>",
            data: "tipe=" + tipe,
            success: function(html) {
                json = eval(html);
                $("#selectbaranggas" + urutan + "").append('<option value="">Pilihan</option>');
                $(json).each(function() {
                    $("#selectbaranggas" + urutan + " ").append('<option value="' + this.kode + '">' + this.disp + "</option>")
                });
            }
        })
    }

    function loadBarang(urutan) {
        var tipe = $("#selectjenis" + urutan).val();
        var opt = "";
            opt += "<option value=''>- Pilihan -</option>";
            opt += "<option value=1>10 Liter</option>";
            opt += "<option value=2>20 Liter</option>";
            opt += "<option value=3>30 Liter</option>";
            $("#selectjumlah" + urutan + " ").remove();
            $("#selectbarang" + urutan + " ").remove();
            $("#barang" + urutan).append('<select class="select2 pilihbarang' + urutan + ' form-control" id="selectbarang' + urutan + '" name="barang[]"></select>');
            $("#jumlah" + urutan).append('<select class="select2 pilihjumlah form-control" id="selectjumlah' + urutan + '" name="jumlah[]"></select>');
            $("#selectjumlah" + urutan + " ").append(opt);
        $.ajax({
            type: 'GET',
            url: "<?= base_url('antrian/getBarang'); ?>",
            data: "tipe=" + tipe,
            success: function(html) {
                json = eval(html);
                $("#selectbarang" + urutan + "").append('<option value="">Pilihan</option>');
                $(json).each(function() {
                    $("#selectbarang" + urutan + " ").append('<option value="' + this.kode + '">' + this.disp + "</option>")
                });
            }
        })
    }

    function loadSPBU() {
        var kec = $("#kecamatan").val();
        $.ajax({
            type: 'GET',
            url: "<?= base_url('antrian/getSPBU'); ?>",
            data: "kode=" + kec,
            success: function(html) {
                $("#spbuArea").html(html);
                $('.spbu').select2();
            }
        });
    }

    function loadAgen() {
        var kec = $("#kecamatan1").val();
        $.ajax({
            type: 'GET',
            url: "<?= base_url('antrian/getAgen'); ?>",
            data: "kode=" + kec,
            success: function(html) {
                $("#agenArea").html(html);
                $('.agen').select2();
            }
        });
    }
    
</script>