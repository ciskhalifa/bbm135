
<?php 

/* Name     : Christiantinus Nesi
 * Email    : christiantinusnesi@gmail.com
 * Created By : Christiantinus Nesi
 */

class Report extends MX_Controller
{

    function __construct()
    {
        parent::__construct();
        if (!isset($_SESSION['username'])) {
            redirect('../login');
        }
        date_default_timezone_set('Asia/Bangkok');
    }

    public function index()
    {
        $data['js'] = 'js';
        $data['css'] = 'css';
        $data['content'] = 'report';
        $data['tabel'] = 'm_tiket';
        $data['kolom'] = array("No", "Perner ID", "Nama Lengkap", "Vendor", "Start Join", "EOF Contract", "Jabatan", "Pendidikan", "Opsi");
        $data['jmlkolom'] = count($data['kolom']);

        $this->load->view('default', $data);
    }

    public function listData()
    {
        if (IS_AJAX) {
            $aColumns = array("no", "kode", "employee_name", "vendor", "start_join", "eof_contract", "nama_jabatan", "pendidikan", "kode");
            $sIndexColumn = "kode";
            $sTable = 'v' . $this->uri->segment(3);
            $sTablex = '';
            // $sWhere = ($_SESSION['kode_prodi'] <> '') ? " AND kode_prodi='".$_SESSION['kode_prodi']."' " : '';
            $sWhere = "";
            $sGroup = "";
            $tQuery = "SELECT * FROM (SELECT @row := @row + 1 AS no, $sTable.*, '' as opsi FROM $sTable, (SELECT @row := 0) AS r) AS tab WHERE 1=1 $sWhere $sGroup";

            echo $this->libglobal->pagingData($aColumns, $sIndexColumn, $sTable, $tQuery, $sTablex);
        }
    }

    public function importExcel()
    {
        $basepath = BASEPATH;
        $stringreplace = str_replace("system", "publik/doc/", $basepath);
        $config['upload_path'] = $stringreplace;
        $config['allowed_types'] = 'xls|xlsx';
        $config['overwrite'] = TRUE;
        $this->load->library('upload', $config);
        $files = $_FILES;
        $_FILES['userfile']['name'] = $files['file']['name'];
        $_FILES['userfile']['type'] = $files['file']['type'];
        $_FILES['userfile']['tmp_name'] = $files['file']['tmp_name'];
        $_FILES['userfile']['error'] = $files['file']['error'];
        $_FILES['userfile']['size'] = $files['file']['size'];
        $new_name = str_replace(" ", "_", $_FILES["file"]['name']);
        $config['file_name'] = $new_name;
        $this->upload->initialize($config);
        if ($this->upload->do_upload()) {
            $this->prosesXLS($new_name);
        } else {
            $error = array('error' => $this->upload->display_errors());
        }
    }

    function prosesXLS($namafile)
    {
        error_reporting(E_ALL & ~E_NOTICE);
        $urutankode = 1;
        $file = 'publik/doc/' . $namafile;
        /*
         * load the excel library
         */
        $this->load->library('excel');
        /*
         * read file from path
         */
        $objPHPExcel = PHPExcel_IOFactory::load($file);
        /*
         * get only the Cell Collection
         */
        $cell_collection = $objPHPExcel->getActiveSheet()->getCellCollection();
        /*
         * extract to a PHP readable array format
         */

        foreach ($cell_collection as $cell) {
            $column = $objPHPExcel->getActiveSheet()->getCell($cell)->getColumn();
            $row = $objPHPExcel->getActiveSheet()->getCell($cell)->getRow();
            $data_value = $objPHPExcel->getActiveSheet()->getCell($cell)->getValue();
            $data_type = $objPHPExcel->getActiveSheet()->getCell($cell)->getFormattedValue();

            /*
             * header will/should be in row 1 only. of course this can be modified to suit your need.
             */
            if ($row <= 1) {
                $header[$row][$column] = $data_value;
            } else {
                $arr_data[$row][$column] = $data_value;
            }
        }

        $kolom = array("no","ticket_id", "date_in", "channel", "pickup_time", "submit_time", "phone", "email", "name", "category", "sub_cat_1", "sub_cat_2", "action", "status", "subject", "remarks", "feedback", "pic", "action", "tujuan_eskalasi", "tgl_close", "email_pic", "status_follow", "sla_close", "sla_opg", "durasi", "cp", "wa", "status_callback", "notes_callback", "mor", "kota", "spbu", "case_owner", "fu_2nd_tier", "submit_2nd_tier", "rt_2nd_tier", "durasi_fu", "bulan");

        foreach ($arr_data as $row) :
            if ($this->cekIsiData($row, $baris) == true) :
                $kode = intval($kl) . $urutankode;
                /*
                 * $x := nilai untuk array $kolom
                 * $k := nilai untuk array xls
                 */
                $k = 1;
                $x = 0;
                $i = 1;
                $data = array();
                foreach ($row as $col) :
                    if (substr($col, 0, 1) == "=") {
                        $nilai = "";
                    } else {
                        $nilai = ($col == '') ? NULL : $col;
                    }
                    switch ($x):
                        case 0:
                        case 39:
                            break;
                        case 1:
                            $carikode = $this->Data_model->jalankanQuery("SELECT * FROM m_tiket WHERE ticket_id ='$nilai' LIMIT 1", 1);
                            if (empty($carikode)) {
                                $data['ticket_id'] = $nilai;
                                $tanda['flag'] = 0;
                            } else {
                                $tanda['flag'] = 1;
                                $data['ticket_id'] = $nilai;
                            }
                            break;
                        case 20:
                            $UNIX_DATE = ($nilai - 25569) * 86400;
                            if ($UNIX_DATE < 0) {
                                $data['tgl_close'] = $this->libglobal->dateReverse($nilai);
                            } else {
                                $data['tgl_close'] = ($nilai == '') ? NULL : gmdate("Y-m-d H:i:s", $UNIX_DATE);
                            }
                            break;
                        case 34:
                        case 35:
                            $UNIX_DATE = ($nilai - 25569) * 86400;
                            if ($UNIX_DATE < 0) {
                                $data[$kolom[$x]] = $this->libglobal->dateReverse($nilai);
                            } else {
                                $data[$kolom[$x]] = ($nilai == '') ? NULL : gmdate("Y-m-d H:i:s", $UNIX_DATE);
                            }
                            break;
                        case 38:
                            $data['bulan'] = $this->input->post('bulan');
                            break;
                        default:
                            $data[$kolom[$x]] = $nilai;
                            break;
                    endswitch;
                    $x++;

                endforeach;
                $i++;
                try {
                    print_r($data);
                    if ($data['tgl_close'] == '--'){
                        $data['sla_close'] = NULL;
                    } else {
                        $date1 = date_create($data['tgl_close']);
                        $date2 = date_create($data['date_in']);
                        $diff = date_diff($date1, $date2);
                        $data['sla_close'] = $diff->d;
                    }

                    if ($data['tgl_close'] !== '--'){
                        $data['sla_opg'] = NULL;
                    } else {
                        $date1 = date_create($data['date_in']);
                        $x = date('Y-m-d H:i:s');
                        $date2 = date_create($x);
                        $diff = date_diff($date1, $date2);
                        $data['sla_opg'] = $diff->days;
                    }

                    if ($tanda['flag'] == 1) {
                        $kondisi = array('ticket_id' => $data['ticket_id']);
                        $this->Data_model->updateDataWhere($data, 'm_tiket', $kondisi);
                        //echo $this->db->last_query();
                    } else {
                        $this->Data_model->simpanData($data, 'm_tiket');
                        //echo $this->db->last_query();
                    }                    
                } catch (Exception $exc) {
                    $data['infoerror'] = $exc->getMessage();
                } else :

            endif;

        endforeach;
    }

    function cekIsiData($row, $baris)
    {
        $isi = false;
        foreach ($row as $eusi) {
            $isi = (strlen(trim($eusi)) > 0) ? true : (($isi == true) ? $isi : false);
        }
        return $isi;
    }
}
