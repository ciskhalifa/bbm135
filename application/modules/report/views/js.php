<!-- Latest compiled and minified JavaScript -->
<script src="<?= base_url() ?>assets/admin/js/formValidation.min.js" type="text/javascript"></script>
<script src="<?= base_url() ?>assets/admin/js/validator.min.js" type="text/javascript"></script>
<script src="<?= base_url() ?>assets/admin/js/jquery.isloading.min.js" type="text/javascript"></script>
<script src="<?= base_url() ?>assets/admin/vendor/datatables/jquery.dataTables.min.js" type="text/javascript"></script>
<script src="<?= base_url() ?>assets/admin/vendor/datatables/dataTables.bootstrap4.min.js"></script>
<script src="<?= base_url() ?>assets/admin/js/dataTables.buttons.min.js" type="text/javascript"></script>
<script src="<?= base_url() ?>assets/admin/js/jszip.min.js" type="text/javascript"></script>
<script src="<?= base_url() ?>assets/admin/js/pdfmake.min.js" type="text/javascript"></script>
<script src="<?= base_url() ?>assets/admin/js/vfs_fonts.js" type="text/javascript"></script>
<script src="<?= base_url() ?>assets/admin/js/buttons.html5.min.js" type="text/javascript"></script>
<script src="<?= base_url() ?>assets/admin/js/buttons.print.min.js" type="text/javascript"></script>

<div id="myConfirm" class="modal animated--grow-in">
    <div class="modal-success">
        <div class="modal-dialog">
            <div class="modal-content">
                <div class="modal-header">
                    <h4 class="modal-title">Konfirmasi</h4>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">×</span></button>
                </div>
                <div class="modal-body">
                    <p>Apakah anda akan melakukan <span class="lblModal h4"></span> ?</p>
                </div>
                <div class="modal-footer">
                    <input type="hidden" id="cid"><input type="hidden" id="cod"><input type="hidden" id="getto">
                    <button type="button" class="btn btn-primary pull-left" data-dismiss="modal">Batal</button>
                    <button type="button" id="btnYes" class="btn btn-danger">Hapus</button>
                </div>
            </div>
        </div>
    </div>
</div>

<script>
    function readURL(input) {
        if (input.files && input.files[0]) {
            var reader = new FileReader();
            reader.onload = function(e) {
                $('#preview').attr('src', e.target.result);
            }
            reader.readAsDataURL(input.files[0]);
        }
    }

    var myApp = myApp || {};
    $(function() {

        $('#import').on('click', function() {
            $('#modalImport').modal();
        });
        $('#report').on('click', function() {
            $('#modalExport').modal();
        });
        $("#file").on("change", function() {
            var file = this.files[0];
            var fileName = file.name;
            var fileType = file.type;
            console.log(fileType);
            if (fileType == "application/vnd.openxmlformats-officedocument.spreadsheetml.sheet" || fileType == "application/vnd.ms-excel") {
                $('#label').text(fileName);
                $('#reset').removeClass('hidden');
            } else {
                alert("Maaf format dokumen tidak sesuai");
                $(this).val('');
                $('#label').text('Pilih File');
            }
        });
        $('#sendForm').validator().on('submit', function(e) {
            if (e.isDefaultPrevented()) {
                // handle the invalid form...
            } else {
                var link = 'report/importExcel/';
                var data = new FormData(this);
                $.ajax({
                    sync: true,
                    url: link,
                    data: data,
                    type: 'POST',
                    datatype: 'html',
                    cache: false,
                    contentType: false,
                    processData: false,
                    beforeSend: function() {
                        $("body").isLoading({
                            text: "",
                            position: "overlay",
                            tpl: '<span class="isloading-wrapper %wrapper%" style="background:none;">%text%<div class="preloader pls-amber" style="position: absolute; top: 0px; left: -40px;"><svg class="pl-circular" viewBox="25 25 50 50"><circle class="plc-path" cx="50" cy="50" r="20"/></svg></div>'
                        });
                    },
                    success: function(html) {
                        setTimeout(function() {
                            // myApp.oTable.fnDraw(false);
                            scrollTo();
                            $('#modalImport').modal('hide');
                            notify('Data berhasil disimpan!', 'success');
                        }, 500);
                        $("body").isLoading("hide");
                        //                        $('#export').modal("hide");
                    },
                    error: function() {
                        notify('Data gagal disimpan!', 'warning');
                        $('#modalImport').modal('hide');
                        $("body").isLoading("hide");
                    },
                });
                return false;
            }
        });
        $('#containerform').hide();
        $('#containerdetail').hide();
        $('#openform').on('click', function() {
            $('#tabelpegawai').hide();
            $('#formpegawai').load('pegawai/form/' + $("#tabel").val());
            $('#containerform').fadeIn('fast');
        });
        $("#btnYes").bind("click", function() {
            var link = $("#getto").val();
            $.ajax({
                url: link,
                type: "POST",
                data: "cid=" + $("#cid").val() + "&cod=" + $("#tabel").val(),
                dataType: "html",
                beforeSend: function() {
                    if (link != "#") {}
                },
                success: function(html) {
                    myApp.oTable.fnDraw(false);
                    $("#myConfirm").modal("hide")
                }
            })
        });

    });
</script>