<div class="container-fluid">
    <div id="tabelpegawai" class="slideInDown animated--grow-in" data-appear="appear" data-animation="slideInDown">
        <input type="hidden" id="tabel" value="<?php echo $tabel; ?>">
        <input type="hidden" id="kolom" value="<?php echo $jmlkolom; ?>">
        <div class="content-detached">
            <div class="content-body">
                <section class="row">
                    <div class="col-md-12">
                        <div class="card shadow mb-2">
                            <div class="card-header py-3">
                                <h4 class="m-0 font-weight-bold text-primary"> Import <?= $this->uri->segment('1'); ?></h4>
                                <a class="heading-elements-toggle"><i class="icon-arrow-right-4"></i></a>
                                <div class="box-tools pull-right">
                                    <a href="javascript:;" class="btn btn-warning btn-sm" id="import" data-tab="" data-original-title="Import Data" data-trigger="hover" data-toggle="tooltip" data-placement="bottom" title="">
                                        <i class="fa fa-upload"></i>
                                    </a>
                                </div>
                            </div>
                            <div class="card-body">
                                
                            </div>
                        </div>
                    </div>
                </section>
            </div>
        </div>
    </div>
    <div class="modal fade text-xs-left animated--grow-in" id="modalImport" tabindex="-1" role="dialog" aria-labelledby="myModalLabel1" style="display: none;" aria-hidden="true">
        <div class="modal-dialog" role="document">
            <div class="modal-content">
                <form role="form" id="sendForm" enctype="multipart/form-data" class="form-horizontal">
                    <!--<form id="sendForm" action="dosen/exportExcel" method="POST" enctype="multipart/form-data">-->
                    <div class="modal-header">
                        <h4 class="modal-title">Import Data</h4>
                    </div>
                    <div class="modal-body">
                        <div class="form-group">
                            <select name="bulan" class="form-control">
                                <option value="">Pilih Bulan</option>
                                <option value="1">Januari</option>
                                <option value="2">Februari</option>
                                <option value="3">Maret</option>
                                <option value="4">April</option>
                                <option value="5">Mei</option>
                                <option value="6">Juni</option>
                                <option value="7">Juli</option>
                                <option value="8">Agustus</option>
                                <option value="9">September</option>
                                <option value="10">Oktober</option>
                                <option value="11">November</option>
                                <option value="12">Desember</option>
                            </select>
                        </div>
                        <div class="form-group">
                            <input id="file" class="form-control" name="file" type="file" style="" required>
                        </div>
                    </div>
                    <div class="modal-footer">
                        <button type="submit" class="btn btn-primary">Upload</button>
                        <button type="reset" class="btn btn-danger" data-dismiss="modal">Tutup</button>
                    </div>
                </form>
            </div>
        </div>
    </div>
</div>