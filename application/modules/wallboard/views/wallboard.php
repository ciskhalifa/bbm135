<input type="hidden" id="kolom" value="<?php echo $jmlkolom; ?>">
<!-- Begin Page Content -->
<div class="container-fluid">
    <!-- Content Row -->
    <div class="row">
        <!-- Earnings (Monthly) Card Example -->
        <div class="col-xl-12 col-md-12 mb-4  animated--grow-in">
            <div class="row">
                <!-- Content Column -->
                <div class="col-lg-4 mb-4">
                    <div class="card shadow mb-4">
                        <div class="card-body">
                            <!-- SPBU Card Example -->
                            <table id="data-spbu" class="table table-hover table-striped">
                                <thead>
                                    <?php
                                    if ($kolom) {
                                        foreach ($kolom as $key => $value) {
                                            if (strlen($value) == 0) {
                                                echo '<th data-type="numeric" class=""></th>';
                                            } else {
                                                echo '<th data-column-id="' . $key . '" data-type="numeric" class="">' . $value . '</th>';
                                            }
                                        }
                                    }
                                    ?>
                                </thead>
                                <tbody>
                                </tbody>
                            </table>
                        </div>
                    </div>
                </div>
                <div class="col-lg-8">
                    <h6 class="m-0 font-weight-bold text-primary text-center">DAILY</h6>
                    <br>
                    <div class="row">
                        <div class="col-xl-3 col-md-6 mb-4">
                            <div class="card bg-warning shadow">
                                <div class="card-body">
                                    <div class="row no-gutters align-items-center">
                                        <div class="col mr-2">
                                            <div class="text-xxlarge font-weight-bold text-center text-white text-uppercase mb-1">WAITING</div>
                                            <div class="h4 mb-0 font-weight-bold text-center text-white"><?= $order[0]->antri; ?></div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="col-xl-3 col-md-6 mb-4">
                            <div class="card bg-success shadow">
                                <div class="card-body">
                                    <div class="row no-gutters align-items-center">
                                        <div class="col mr-2">
                                            <div class="text-xxlarge font-weight-bold text-center text-white text-uppercase mb-1">SUCCESS</div>
                                            <div class="h4 mb-0 font-weight-bold text-center text-white"><?= $order[0]->success; ?></div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="col-xl-3 col-md-6 mb-4">
                            <div class="card bg-info shadow">
                                <div class="card-body">
                                    <div class="row no-gutters align-items-center">
                                        <div class="col mr-2">
                                            <div class="text-xxlarge font-weight-bold text-center text-white text-uppercase mb-1">ON PROGRESS</div>
                                            <div class="h4 mb-0 font-weight-bold text-center text-white"><?= $order[0]->progress; ?></div>
                                        </div>

                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="col-xl-3 col-md-6 mb-4">
                            <div class="card bg-primary shadow">
                                <div class="card-body">
                                    <div class="row no-gutters align-items-center">
                                        <div class="col mr-2">
                                            <div class="text-xxlarge font-weight-bold text-center text-white text-uppercase mb-1">ORDER</div>
                                            <div class="h4 mb-0 font-weight-bold text-center text-white"><?= $order[0]->totaldata; ?></div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="col-xl-12 col-md-6 mb-4">
                            <div class="card bg-white shadow">
                            </div>
                        </div>
                        <div class="col-xl-12 col-md-12 mb-4">
                            <h6 class="m-0 font-weight-bold text-primary text-center">MONTHLY</h6>
                            <br>
                            <div class="row">
                                <div class="col-xl-3 col-md-6 mb-4">
                                    <div class="card bg-warning shadow">
                                        <div class="card-body">
                                            <div class="row no-gutters align-items-center">
                                                <div class="col mr-2">
                                                    <div class="text-xxlarge font-weight-bold text-center text-white text-uppercase mb-1">WAITING</div>
                                                    <div class="h4 mb-0 font-weight-bold text-center text-white"><?= $monthly[0]->antri; ?></div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div class="col-xl-3 col-md-6 mb-4">
                                    <div class="card bg-success shadow">
                                        <div class="card-body">
                                            <div class="row no-gutters align-items-center">
                                                <div class="col mr-2">
                                                    <div class="text-xxlarge font-weight-bold text-center text-white text-uppercase mb-1">SUCCESS</div>
                                                    <div class="h4 mb-0 font-weight-bold text-center text-white"><?= $monthly[0]->success; ?></div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div class="col-xl-3 col-md-6 mb-4">
                                    <div class="card bg-info shadow">
                                        <div class="card-body">
                                            <div class="row no-gutters align-items-center">
                                                <div class="col mr-2">
                                                    <div class="text-xxlarge font-weight-bold text-center text-white text-uppercase mb-1">ON PROGRESS</div>
                                                    <div class="h4 mb-0 font-weight-bold text-center text-white"><?= $monthly[0]->progress; ?></div>
                                                </div>

                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div class="col-xl-3 col-md-6 mb-4">
                                    <div class="card bg-primary shadow">
                                        <div class="card-body">
                                            <div class="row no-gutters align-items-center">
                                                <div class="col mr-2">
                                                    <div class="text-xxlarge font-weight-bold text-center text-white text-uppercase mb-1">ORDER</div>
                                                    <div class="h4 mb-0 font-weight-bold text-center text-white"><?= $monthly[0]->totaldata; ?></div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="col-xl-12 col-md-6 mb-4">
                            <div class="card bg-white shadow">
                            </div>
                        </div>
                        <div class="col-xl-12 col-md-12 mb-4">
                            <h6 class="m-0 font-weight-bold text-primary text-center">YEARLY</h6>
                            <br>
                            <div class="row">
                                <div class="col-xl-3 col-md-6 mb-4">
                                    <div class="card bg-warning shadow">
                                        <div class="card-body">
                                            <div class="row no-gutters align-items-center">
                                                <div class="col mr-2">
                                                    <div class="text-xxlarge font-weight-bold text-center text-white text-uppercase mb-1">WAITING</div>
                                                    <div class="h4 mb-0 font-weight-bold text-center text-white"><?= $yearly[0]->antri; ?></div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div class="col-xl-3 col-md-6 mb-4">
                                    <div class="card bg-success shadow">
                                        <div class="card-body">
                                            <div class="row no-gutters align-items-center">
                                                <div class="col mr-2">
                                                    <div class="text-xxlarge font-weight-bold text-center text-white text-uppercase mb-1">SUCCESS</div>
                                                    <div class="h4 mb-0 font-weight-bold text-center text-white"><?= $yearly[0]->success; ?></div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div class="col-xl-3 col-md-6 mb-4">
                                    <div class="card bg-info shadow">
                                        <div class="card-body">
                                            <div class="row no-gutters align-items-center">
                                                <div class="col mr-2">
                                                    <div class="text-xxlarge font-weight-bold text-center text-white text-uppercase mb-1">ON PROGRESS</div>
                                                    <div class="h4 mb-0 font-weight-bold text-center text-white"><?= $yearly[0]->progress; ?></div>
                                                </div>

                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div class="col-xl-3 col-md-6 mb-4">
                                    <div class="card bg-primary shadow">
                                        <div class="card-body">
                                            <div class="row no-gutters align-items-center">
                                                <div class="col mr-2">
                                                    <div class="text-xxlarge font-weight-bold text-center text-white text-uppercase mb-1">ORDER</div>
                                                    <div class="h4 mb-0 font-weight-bold text-center text-white"><?= $yearly[0]->totaldata; ?></div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
<!-- Earnings (Monthly) Card Example -->
<!-- /.container-fluid -->