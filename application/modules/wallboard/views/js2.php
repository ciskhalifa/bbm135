<script src="<?= base_url('assets/admin') ?>/vendor/datatables/jquery.dataTables.min.js"></script>
<script src="<?= base_url('assets/admin') ?>/vendor/datatables/dataTables.bootstrap4.min.js"></script>
<script src="<?= base_url('assets/admin') ?>/js/demo/datatables-demo.js"></script>
<script src="https://cdn.jsdelivr.net/npm/chart.js@2.8.0"></script>
<script src="https://cdn.jsdelivr.net/npm/chartjs-plugin-datalabels@0.7.0"></script>
<script>
    $(function() {
        $('#data-spbu').dataTable({
            dom: "<'row'<'col-md-5'l><'col-md-7'f>r<'clear'>>t<'row'<'col-md-6'i><'col-md-6'p>>",
            bProcessing: true,
            bServerSide: true,
            retrieve: true,
            responsive: false,
            lengthChange: false,
            searching: false,
            ordering: false,
            paging: false,
            autoWidth: false,
            info: false
        });
    });
</script>
<script>
    $(function() {
        // CHART ORDER ITEM
        var options = {
            responsive: true,
            maintainAspectRatio: true,
            tooltips: {
                enabled: true,
                mode: 'nearest'
            },

            plugins: {
                datalabels: {
                    align: 'end',
                    anchor: 'center',
                    borderRadius: 25,
                    borderWidth: 2,
                    color: function(ctx) {
                        return 'white';
                    },
                    font: function(context) {
                        var w = context.chart.width;
                        return {
                            size: w < 512 ? 12 : 14,
                            weight: 'bold'
                        };
                    },

                }
            },
        };

        var options2 = {
            tooltips: {
                mode: 'index',
                intersect: false
            },
            responsive: true,
            scales: {
                xAxes: [{
                    stacked: true,
                }],
                yAxes: [{
                    stacked: true
                }]
            },
            plugins: {
                datalabels: {
                    align: 'start',
                    anchor: 'center',
                    borderRadius: 25,
                    borderWidth: 2,
                    color: function(ctx) {
                        return 'white';
                    },
                    font: function(context) {
                        var w = context.chart.width;
                        return {
                            size: w < 512 ? 12 : 14,
                            weight: 'bold'
                        };
                    },

                }
            },
        };


        var ctx = document.getElementById('myChart').getContext('2d');
        var ctx2 = document.getElementById('myChart2').getContext('2d');
        var ctx3 = document.getElementById('myChart3').getContext('2d');
        var ctx4 = document.getElementById('myChart4').getContext('2d');

        // var ctx5 = document.getElementById('myChart5').getContext('2d');
        var ctx6 = document.getElementById('myChart6').getContext('2d');

        var ctx7 = document.getElementById('myChart7').getContext('2d');
        var ctx8 = document.getElementById('myChart8').getContext('2d');
        var ctx9 = document.getElementById('myChart9').getContext('2d');

        var ctx10 = document.getElementById('myChart10').getContext('2d');


        var chart = new Chart(ctx, {
            // The type of chart we want to create
            type: 'doughnut',
            // The data for our dataset
            data: {
                labels: ["On Progress", "Closed", "Blank"],
                datasets: [{
                    label: 'STATUS FOLLOW UP TIKET',
                    backgroundColor: [
                        '#00e366',
                        '#036ffc',
                        '#EB2815',
                        '#347B98',
                        '#092834',
                        '#E85302'
                    ],
                    borderColor: [
                        '#00e366',
                        '#036ffc',
                        '#EB2815',
                        '#347B98',
                        '#092834',
                        '#E85302'
                    ],
                    data: [<?= $status_follow[0]->progress ?>, <?= $status_follow[0]->close ?>, <?= $status_follow[0]->blank ?>]
                }]
            },

            // Configuration options go here
            options: options
        });

        var barChartData = {
            labels: [<?php for ($i = 0; $i < count($status_mor); $i++) {
                            echo '"' . $status_mor[$i]->mor . '",';
                        } ?>],
            datasets: [{
                    label: "Closed",
                    backgroundColor: '#00e366',
                    stack: 'Stack 0',
                    data: [
                        <?php for ($i = 0; $i < count($status_mor); $i++) {
                            echo '"' . $status_mor[$i]->close . '",';
                        } ?>
                    ]
                },
                {
                    label: "On Progress",
                    backgroundColor: '#036ffc',
                    stack: 'Stack 0',
                    data: [
                        <?php for ($i = 0; $i < count($status_mor); $i++) {
                            echo '"' . $status_mor[$i]->progress . '",';
                        } ?>
                    ]
                }
            ]
        };

        var chart2 = new Chart(ctx2, {
            // The type of chart we want to create
            type: 'bar',
            // The data for our dataset
            data: barChartData,
            // Configuration options go here
            options: options2
        });

        var horizontalBar = {
            labels: [<?php for ($i = 0; $i < count($sla_close); $i++) {
                            echo '"' . $sla_close[$i]->mor . '",';
                        } ?>],
            datasets: [{
                    label: "% In SLA",
                    backgroundColor: '#00e366',
                    stack: 'Stack 0',
                    data: [
                        <?php for ($i = 0; $i < count($sla_close); $i++) {
                            $total = $sla_close[$i]->in_sla + $sla_close[$i]->over_sla;
                            $x = ($sla_close[$i]->in_sla / $total) * 100;
                            echo '"' . round($x, 2) . '",';
                        } ?>
                    ]
                },
                {
                    label: "% Over SLA",
                    backgroundColor: '#036ffc',
                    stack: 'Stack 0',
                    data: [
                        <?php for ($i = 0; $i < count($sla_close); $i++) {
                            $total = $sla_close[$i]->in_sla + $sla_close[$i]->over_sla;
                            $x = ($sla_close[$i]->over_sla / $total) * 100;
                            echo '"' . round($x, 2) . '",';
                        } ?>
                    ]
                }
            ]
        };

        var chart3 = new Chart(ctx3, {
            // The type of chart we want to create
            type: 'horizontalBar',
            // The data for our dataset
            data: horizontalBar,
            // Configuration options go here
            options: options2
        })

        var chart4 = new Chart(ctx4, {
            // The type of chart we want to create
            type: 'bar',
            // The data for our dataset
            data: {
                labels: [<?php for ($i = 0; $i < count($progress_pic); $i++) {
                                echo '"' . $progress_pic[$i]->email_pic . '",';
                            } ?>],
                datasets: [{
                    label: 'On Progress',
                    backgroundColor: '#036ffc',
                    borderColor: '#036ffc',
                    data: [<?php for ($i = 0; $i < count($progress_pic); $i++) {
                                echo '"' . $progress_pic[$i]->progress . '",';
                            } ?>]
                }]
            },

            // Configuration options go here
            options: options
        });

        // var chart5;

        var barChartData6 = {
            labels: [<?php for ($i = 0; $i < count($tiket_per_bulan); $i++) {
                            echo '"' . $tiket_per_bulan[$i]->bulan . '",';
                        } ?>],
            datasets: [{
                    label: "Closed In SLA",
                    backgroundColor: '#00e366',
                    stack: 'Stack 0',
                    data: [
                        <?php for ($i = 0; $i < count($tiket_per_bulan); $i++) {
                            echo '"' . $tiket_per_bulan[$i]->in_sla . '",';
                        } ?>
                    ]
                },
                {
                    label: "Closed Out SLA",
                    backgroundColor: '#036ffc',
                    stack: 'Stack 1',
                    data: [
                        <?php for ($i = 0; $i < count($tiket_per_bulan); $i++) {
                            echo '"' . $tiket_per_bulan[$i]->over_sla . '",';
                        } ?>
                    ]
                },
                {
                    label: "On Progress",
                    backgroundColor: '#03630d',
                    stack: 'Stack 2',
                    data: [
                        <?php for ($i = 0; $i < count($tiket_per_bulan); $i++) {
                            echo '"' . $tiket_per_bulan[$i]->progress . '",';
                        } ?>
                    ]
                }
            ]
        };

        var chart6 = new Chart(ctx6, {
            // The type of chart we want to create
            type: 'bar',
            // The data for our dataset
            data: barChartData6,
            // Configuration options go here
            options: options2
        });

        var chart8 = new Chart(ctx8, {
            // The type of chart we want to create
            type: 'pie',
            // The data for our dataset
            data: {
                labels: ["On Progress", "Closed"],
                datasets: [{
                    label: 'STATUS TIKET',
                    backgroundColor: [
                        '#00e366',
                        '#036ffc'
                    ],
                    borderColor: [
                        '#00e366',
                        '#036ffc'
                    ],
                    data: [<?= round($status_tiket[0]->progress) ?>, <?= round($status_tiket[0]->closed) ?>]
                }]
            },

            // Configuration options go here
            options: options
        });

        var chart9 = new Chart(ctx9, {
            // The type of chart we want to create
            type: 'pie',
            // The data for our dataset
            data: {
                labels: ["On Progress", "Closed"],
                datasets: [{
                    label: 'Performasi SLA',
                    backgroundColor: [
                        '#00e366',
                        '#036ffc'
                    ],
                    borderColor: [
                        '#00e366',
                        '#036ffc'
                    ],
                    data: [<?= round($performansi_sla[0]->close_in_sla) ?>, <?= round($performansi_sla[0]->over_sla) ?>]
                }]
            },

            // Configuration options go here
            options: options
        });

        var barChartData10 = {
            labels: [<?php for ($i = 0; $i < count($eskalasi); $i++) {
                            echo '"' . $eskalasi[$i]->tujuan_eskalasi . '",';
                        } ?>],
            datasets: [{
                    label: "Total",
                    backgroundColor: '#036ffc',
                    stack: 'Stack 0',
                    data: [
                        <?php for ($i = 0; $i < count($eskalasi); $i++) {
                            echo '"' . $eskalasi[$i]->total . '",';
                        } ?>
                    ]
                },
                {
                    label: "Close",
                    backgroundColor: '#00e366',
                    stack: 'Stack 0',
                    data: [
                        <?php for ($i = 0; $i < count($eskalasi); $i++) {
                            echo '"' . $eskalasi[$i]->closed . '",';
                        } ?>
                    ]
                },
                {
                    label: "On Progress",
                    backgroundColor: '#e60000',
                    stack: 'Stack 0',
                    data: [
                        <?php for ($i = 0; $i < count($eskalasi); $i++) {
                            echo '"' . $eskalasi[$i]->progress . '",';
                        } ?>
                    ]
                }
            ]
        };

        var chart10 = new Chart(ctx10, {
            // The type of chart we want to create
            type: 'bar',
            // The data for our dataset
            data: barChartData10,
            // Configuration options go here
            options: options2
        });



    });
</script>