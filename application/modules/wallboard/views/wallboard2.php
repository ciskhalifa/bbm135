<!-- Begin Page Content -->
<div class="container-fluid">
    <!-- Content Row -->
    <div class="row">
        <!-- Earnings (Monthly) Card Example -->
        <div class="col-xl-12 col-md-12 mb-4  animated--grow-in">
            <!-- STATUS FOLLOW UP -->
            <div class="col-lg-12">
                <div class="row">
                    <div class="col-lg-6">
                        <!-- ORDER ITEM CHART -->
                        <div class="card shadow mb-4">
                            <div class="card-header py-3">
                                <h6 class="m-0 font-weight-bold text-primary text-center">STATUS FOLLOW UP TIKET <?= date('F Y'); ?></h6>
                            </div>
                            <div class="card-body">
                                <canvas id="myChart"></canvas>
                            </div>
                        </div>
                    </div>
                    <div class="col-lg-6">
                        <!-- ORDER ITEM CHART -->
                        <div class="card shadow mb-4">
                            <div class="card-header py-3">
                                <h6 class="m-0 font-weight-bold text-primary text-center">STATUS FU TIKET PER MOR <?= date('F Y'); ?></h6>
                            </div>
                            <div class="card-body">
                                <canvas id="myChart2"></canvas>
                            </div>
                        </div>
                    </div>
                </div>
            </div>

            <!-- STATUS TIKET CLOSE -->
            <div class="col-lg-12">
                <div class="row">
                    <div class="col-lg-12">
                        <!-- ORDER ITEM CHART -->
                        <div class="card shadow mb-4">
                            <div class="card-header py-3">
                                <h6 class="m-0 font-weight-bold text-primary text-center">STATUS FU TIKET CLOSE PER MOR <?= date('F Y'); ?></h6>
                            </div>
                            <div class="card-body">
                                <canvas id="myChart3"></canvas>
                            </div>
                        </div>
                    </div>
                </div>
            </div>

            <!-- STATUS TIKET CLOSE -->
            <div class="col-lg-12">
                <div class="row">
                    <div class="col-lg-12">
                        <!-- ORDER ITEM CHART -->
                        <div class="card shadow mb-4">
                            <div class="card-header py-3">
                                <h6 class="m-0 font-weight-bold text-primary text-center">STATUS FU TIKET ON PROGRESS <?= date('F Y'); ?></h6>
                            </div>
                            <div class="card-body">
                                <canvas id="myChart4"></canvas>
                            </div>
                        </div>
                    </div>
                </div>
            </div>

            <!-- REKAP PERFORMANSI -->
            <div class="col-lg-12">
                <div class="row">
                    <div class="col-lg-12">
                        <!-- ORDER ITEM CHART -->
                        <div class="card shadow mb-4">
                            <div class="card-header py-3">
                                <h6 class="m-0 font-weight-bold text-primary text-center">REKAP PERFORMANSI FOLLOW UP TIKET PER MOR <?= date('F Y'); ?></h6>
                            </div>
                            <div class="card-body">
                                <table id="data-spbu" class="table table-bordered table-hover table-striped">
                                    <tr>
                                        <th rowspan="2">MOR</th>
                                        <?php $bulan = $this->Data_model->jalankanQuery("SELECT DISTINCT(bulan) as bulan FROM tes", 3);
                                        foreach ($bulan as $row) : ?>
                                            <th colspan="4" style="text-align: center;"><?= $row->bulan; ?></th>
                                        <?php endforeach; ?>
                                    </tr>
                                    <?php for ($i = 0; $i < count($bulan); $i++) : ?>
                                        <th>Total</th>
                                        <th>Open</th>
                                        <th>Progress</th>
                                        <th>Close</th>
                                    <?php endfor; ?>
                                    <?php foreach ($rekap as $row) : ?>
                                        <tr>
                                            <td><?= $row->mor; ?></td>
                                        </tr>
                                    <?php endforeach; ?>
                                    <tr></tr>
                                </table>
                            </div>
                        </div>
                    </div>

                    <div class="col-lg-6">
                        <!-- ORDER ITEM CHART -->
                        <div class="card shadow mb-4">
                            <div class="card-header py-3">
                                <h6 class="m-0 font-weight-bold text-primary text-center">STATUS TIKET PER BULAN <?= date('F Y'); ?></h6>
                            </div>
                            <div class="card-body">
                                <canvas id="myChart6"></canvas>
                            </div>
                        </div>
                    </div>
                </div>
            </div>

            <!-- TREN TIKET -->
            <div class="col-lg-12">
                <div class="row">
                    <div class="col-lg-6">
                        <!-- ORDER ITEM CHART -->
                        <div class="card shadow mb-4">
                            <div class="card-header py-3">
                                <h6 class="m-0 font-weight-bold text-primary text-center">TREND TIKET <?= date('F Y'); ?></h6>
                            </div>
                            <div class="card-body">
                                <canvas id="myChart7"></canvas>
                            </div>
                        </div>
                    </div>

                    <div class="col-lg-3">
                        <!-- ORDER ITEM CHART -->
                        <div class="card shadow mb-4">
                            <div class="card-header py-3">
                                <h6 class="m-0 font-weight-bold text-primary text-center">STATUS TIKET <?= date('F Y'); ?></h6>
                            </div>
                            <div class="card-body">
                                <canvas id="myChart8"></canvas>
                            </div>
                        </div>
                    </div>
                    <div class="col-lg-3">
                        <!-- ORDER ITEM CHART -->
                        <div class="card shadow mb-4">
                            <div class="card-header py-3">
                                <h6 class="m-0 font-weight-bold text-primary text-center">PERFORMASI SLA <?= date('F Y'); ?></h6>
                            </div>
                            <div class="card-body">
                                <canvas id="myChart9"></canvas>
                            </div>
                        </div>
                    </div>

                    <div class="col-lg-6">
                        <!-- ORDER ITEM CHART -->

                    </div>

                    <div class="col-lg-6">
                        <!-- ORDER ITEM CHART -->
                        <div class="card shadow mb-4">
                            <div class="card-header py-3">
                                <h6 class="m-0 font-weight-bold text-primary text-center">TUJUAN ESKALASI TIKET <?= date('F Y'); ?></h6>
                            </div>
                            <div class="card-body">
                                <canvas id="myChart10"></canvas>
                            </div>
                        </div>
                    </div>
                </div>
            </div>

            <!-- STATUS TIKET CLOSE -->
            <div class="col-lg-12">
                <div class="row">
                    <div class="col-lg-12">
                        <!-- ORDER ITEM CHART -->
                        <div class="card shadow mb-4">
                            <div class="card-header py-3">
                                <h6 class="m-0 font-weight-bold text-primary text-center">PERFORMANSI FOLLOW UP TIKET PER SBM <?= date('F Y'); ?></h6>
                            </div>
                            <div class="card-body">
                                <canvas id="myChart11"></canvas>
                            </div>
                        </div>
                    </div>
                </div>
            </div>

        </div>
    </div>
</div>