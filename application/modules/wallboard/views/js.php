<script src="<?= base_url('assets/admin') ?>/vendor/datatables/jquery.dataTables.min.js"></script>
<script src="<?= base_url('assets/admin') ?>/vendor/datatables/dataTables.bootstrap4.min.js"></script>
<script src="<?= base_url('assets/admin') ?>/js/demo/datatables-demo.js"></script>
<script src="https://cdn.jsdelivr.net/npm/chart.js@2.8.0"></script>
<script src="https://cdn.jsdelivr.net/npm/chartjs-plugin-datalabels@0.7.0"></script>
<script>
    $(function() {
        // CHART ORDER ITEM
        var options = {
            responsive: true,
            maintainAspectRatio: true,
            tooltips: {
                enabled: true,
                mode: 'nearest'
            },

            plugins: {
                datalabels: {
                    align: 'end',
                    anchor: 'center',
                    borderRadius: 25,
                    borderWidth: 2,
                    color: function(ctx) {
                        return 'white';
                    },
                    font: function(context) {
                        var w = context.chart.width;
                        return {
                            size: w < 512 ? 12 : 14,
                            weight: 'bold'
                        };
                    },
                    
                }
                // labels: {
                //     render: 'percentage',
                //     fontColor: '#fff',
                //     fontSize: 14,
                //     fontStyle: 'bold',
                //     precision: 0
                // }
            },
        };
        var ctx = document.getElementById('myChart').getContext('2d');
        var chart = new Chart(ctx, {
            // The type of chart we want to create
            type: 'pie',
            // The data for our dataset
            data: {
                labels: [<?php for ($i = 0; $i < count($label); $i++) {
                                echo '"' . $label[$i]->nama_jenis . '",';
                            } ?>],
                datasets: [{
                    label: 'ORDER ITEM',
                    backgroundColor: [
                        '#C9F0AD',
                        '#B2D732',
                        '#EB2815',
                        '#347B98',
                        '#092834',
                        '#E85302'
                    ],
                    borderColor: [
                        '#C9F0AD',
                        '#B2D732',
                        '#EB2815',
                        '#347B98',
                        '#092834',
                        '#E85302'
                    ],
                    data: [<?php for ($i = 0; $i < count($label); $i++) {
                                echo '"' . $label[$i]->jml . '",';
                            } ?>]
                }]
            },

            // Configuration options go here
            options: options
        });
    });
</script>
<script>
    $(function() {
        var myTable;
        if ($("#kolom").val() > 0) {
            myTable = $('#data-spbu').dataTable({
                dom: "<'row'<'col-md-5'l><'col-md-7'f>r<'clear'>>t<'row'<'col-md-6'i><'col-md-6'p>>",
                bProcessing: true,
                bServerSide: true,
                retrieve: true,
                responsive: false,
                lengthChange: false,
                searching: false,
                ordering: false,
                paging: false,
                autoWidth: false,
                info: false,
                oLanguage: {
                    sLoadingRecords: "Tunggu sejenak - memuat...",
                    sProcessing: '<div style="text-align:center;">Sedang Proses</div>',
                    oPaginate: {
                        sFirst: "<<",
                        sLast: ">>",
                        sNext: ">",
                        sPrevious: "<"
                    }
                },
                sAjaxSource: '<?= base_url('wallboard/getDataSPBU') ?>',
                fnServerData: function(sSource, aoData, fnCallback, oSettings) {
                    oSettings.jqXHR = $.ajax({
                        dataType: "json",
                        type: "POST",
                        url: sSource,
                        data: aoData,
                        success: fnCallback
                    })
                },
                aoColumnDefs: [{
                    aTargets: [2],
                    sClass: "text-center",
                    sWidth: "10px"
                }],
            });
            setInterval(function() {
                myTable.api().ajax.reload();
            }, 300000);
        }
        $(".dataTables_processing").css({
            position: "absolute",
            width: "200px",
            top: "50%",
            left: "50%",
            "margin-left": "-100px",
            "text-align": "center",
            "padding-top": "1em",
            "padding-right": "0px",
            "padding-bottom": "1em",
            "padding-left": "0px"
        });
    });
</script>