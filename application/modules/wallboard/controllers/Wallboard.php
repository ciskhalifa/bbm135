
<?php

/* Name     : Christiantinus Nesi
 * Email    : christiantinusnesi@gmail.com
 * Created By : Christiantinus Nesi
 */

class Wallboard extends MX_Controller
{

    function __construct()
    {
        parent::__construct();
    }

    public function index()
    {
        // MANAGEMENT
        $data['js'] = 'js';
        $data['css'] = 'css';
        $data['content'] = 'wallboard';
        $data['kolom'] = array("No.", "SPBU", "QUOTA");
        $data['jmlkolom'] = count($data['kolom']);
        $data['order'] = $this->Data_model->jalankanQuery("SELECT * FROM vorderdaily", 3);
        $data['monthly'] = $this->Data_model->jalankanQuery("SELECT * FROM vordermonthly", 3);
        $data['yearly'] = $this->Data_model->jalankanQuery("SELECT * FROM vorderyearly", 3);
        $this->db->query("SET @rank = 0");
        $data['topagent'] = $this->Data_model->jalankanQuery("SELECT *,@rank := @rank + 1 AS rank FROM vtopagent ORDER BY rank ASC LIMIT 10", 3);
        $this->db->query("SET @rank = 0");
        $data['topspbu'] = $this->Data_model->jalankanQuery("SELECT *,@rank := @rank + 1 AS rank FROM vtopspbu ORDER BY rank ASC LIMIT 10", 3);
        $data['label'] = $this->Data_model->jalankanQuery("SELECT *, SUM(IF(m_jenis.`tipe` = 'BENSIN', t_antrian_detail.`jumlah` * 10, t_antrian_detail.jumlah)) AS jml FROM t_antrian_detail JOIN m_jenis ON m_jenis.`kode` = t_antrian_detail.`jenis` GROUP BY m_jenis.nama_jenis ORDER BY m_jenis.nama_jenis ASC", 3);

        $this->load->view('default_wall', $data);
    }

    public function index2()
    {
        // MANAGEMENT
        $data['js'] = 'js2';
        $data['css'] = 'css2';
        $data['content'] = 'wallboard2';
        $data['order'] = $this->Data_model->jalankanQuery("SELECT * FROM vorderdaily", 3);
        // STATUS FOLLOW UP
        $data['status_follow'] = $this->Data_model->jalankanQuery("SELECT
        COUNT(IF((`m_tiket`.`status_follow` = 'Closed'),`m_tiket`.`status_follow`,NULL)) AS `close`,
        COUNT(IF((`m_tiket`.`status_follow` = 'On Progress'),`m_tiket`.`status_follow`,NULL)) AS `progress`,
        COUNT(IF((m_tiket.`status_follow` = ''), m_tiket.`status_follow`, NULL)) AS blank, COUNT(0) AS `totaldata` FROM m_tiket", 3);

        // STATUS FOLLOW UP / MOR
        $data['status_mor'] = $this->Data_model->jalankanQuery("SELECT mor,
          COUNT(IF((`m_tiket`.`status_follow` = 'Closed'),`m_tiket`.`status_follow`,NULL)) AS `close`,
          COUNT(IF((`m_tiket`.`status_follow` = 'On Progress'),`m_tiket`.`status_follow`,NULL)) AS `progress`,
          COUNT(IF((m_tiket.`status_follow` = ''), m_tiket.`status_follow`, NULL)) AS blank,
          COUNT(0) AS `totaldata`
        FROM m_tiket
        WHERE mor IS NOT NULL
        GROUP BY mor", 3);

        // SLA CLOSED
        $data['sla_close'] = $this->Data_model->jalankanQuery("SELECT mor, 
        COUNT(IF(m_tiket.`sla_close` <= 3, m_tiket.`status_follow`, NULL)) AS in_sla,
        COUNT(IF(m_tiket.`sla_close` > 3, m_tiket.`status_follow`, NULL)) AS over_sla,
        COUNT(IF(m_tiket.status_follow = 'On Progress', m_tiket.`status_follow`, NULL)) AS progress
        FROM m_tiket
        WHERE mor IS NOT NULL
        GROUP BY mor", 3);

        // ON PROGRESS BY EMAIL
        $data['progress_pic'] = $this->Data_model->jalankanQuery("SELECT SUBSTRING_INDEX(email_pic,'@',1) AS email_pic, 
        COUNT(IF(m_tiket.status_follow = 'On Progress', m_tiket.`status_follow`, NULL)) AS progress
        FROM m_tiket
        WHERE email_pic IS NOT NULL
        GROUP BY `email_pic`", 3);

        // STATUS TIKET PER BULAN
        $data['tiket_per_bulan'] = $this->Data_model->jalankanQuery("SELECT MONTHNAME(date_in) AS bulan,
        COUNT(IF(m_tiket.`sla_close` <= 3, m_tiket.`status_follow`, NULL)) AS in_sla,
        COUNT(IF(m_tiket.`sla_close` > 3, m_tiket.`status_follow`, NULL)) AS over_sla,
        COUNT(IF(m_tiket.status_follow = 'On Progress', m_tiket.`status_follow`, NULL)) AS progress,
        COUNT(0) AS total
        FROM m_tiket
        GROUP BY MONTHNAME(date_in)", 3);

        // REKAP PERFORMANSI
        $data['rekap'] = $this->Data_model->jalankanQuery("SELECT
        mor,
        COUNT(0) AS total,
        SUM(
            CASE
            WHEN status_follow = 'Open'
            THEN 1
            ELSE 0
            END
        ) AS open,
        SUM(
            CASE
            WHEN status_follow = 'On Progress'
            THEN 1
            ELSE 0
            END
        ) AS progress,
        SUM(
            CASE
            WHEN status_follow = 'Closed'
            THEN 1
            ELSE 0
            END
        ) AS closed,
        MONTHNAME(date_in) AS bulan
        FROM
        m_tiket
        WHERE mor IS NOT NULL
        GROUP BY mor, MONTHNAME(date_in)
        ORDER BY MONTHNAME(date_in), mor", 3);

        // TREND TIKET
        //$data['trend_tiket'] = $this->Data_model->jalankanQuery("",3);

        // PERFORMANSI SLA
        $data['performansi_sla'] = $this->Data_model->jalankanQuery("SELECT
        COUNT(IF(m_tiket.`sla_close` <= 3, m_tiket.`status_follow`, NULL)) AS close_in_sla,
        COUNT(IF(m_tiket.`sla_close` > 3, m_tiket.`status_follow`, NULL)) AS over_sla
        FROM m_tiket
        WHERE mor IS NOT NULL AND status_follow = 'Closed'", 3);

        // STATUS TIKET
        $data['status_tiket'] = $this->Data_model->jalankanQuery("SELECT 
        COUNT(IF(m_tiket.`status_follow` = 'On Progress', m_tiket.`status_follow`, NULL)) AS progress,
        COUNT(IF(m_tiket.`status_follow` = 'Closed', m_tiket.`status_follow`, NULL)) AS closed
        FROM m_tiket", 3);

        // TUJUAN ESKALASI
        $data['eskalasi'] = $this->Data_model->jalankanQuery("SELECT tujuan_eskalasi,
        COUNT(IF(m_tiket.`status_follow` = 'Closed', m_tiket.`status_follow`, NULL)) AS closed,
        COUNT(IF(m_tiket.status_follow = 'On Progress', m_tiket.`status_follow`, NULL)) AS progress,
        COUNT(0) AS total
        FROM m_tiket
        WHERE tujuan_eskalasi IS NOT NULL
        GROUP BY tujuan_eskalasi", 3);

        $this->load->view('default_wall', $data);
    }

    function getDataSPBU()
    {
        if (IS_AJAX) {
            $sTablex = "";
            // $sTable = 'm_spbu'; // m_barang
            $sTable = 'm_spbu';
            $aColumns = array("kode", "nama", "quota");
            $kolom = "kode,nama,quota";
            $where = " 1=1";
            $sIndexColumn = "kode";
            if (isset($kolom) && strlen($kolom) > 0) {
                $tQuery = "SELECT $kolom "
                    . "FROM $sTable a $sTablex WHERE $where ";
                echo $this->libglobal->pagingData($aColumns, $sIndexColumn, $sTable, $tQuery, $sTablex);
            }
        } else {
            echo "";
        }
    }
}
