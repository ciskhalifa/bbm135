
<?php

/* Name     : Christiantinus Nesi
 * Email    : christiantinusnesi@gmail.com
 * Created By : Christiantinus Nesi
 */

class Dashboard extends MX_Controller
{

    function __construct()
    {
        parent::__construct();
        if (!isset($_SESSION['username'])) {
            redirect('../login');
        }
    }

    public function index()
    {
        if ($_SESSION['role'] == '1') {
            // AGENT
            $data['js'] = 'js';
            $data['css'] = 'css';
            $data['content'] = 'dashboard';
            $data['kolom'] = array("No.", "DAERAH", "SPBU 1", "QUOTA 1", "SPBU 2", "QUOTA 2");
            $data['jmlkolom'] = count($data['kolom']);
            $data['rowdata'] = $this->Data_model->jalankanQuery("SELECT * FROM vspbu", 3);
            $data['antrian'] = $this->Data_model->getTotalData('t_antrian', array('agent' => $_SESSION['kode']));
            $data['antri'] = $this->Data_model->getTotalData('t_antrian', array('agent' => $_SESSION['kode'], 'status' => 'ANTRI'));
            $data['success'] = $this->Data_model->getTotalData('t_antrian', array('agent' => $_SESSION['kode'], 'status' => 'SUCCESS'));
            $data['cancel'] = $this->Data_model->getTotalData('t_antrian', array('agent' => $_SESSION['kode'], 'status' => 'CANCEL'));
        
        } else if ($_SESSION['role'] == '2') {
            // COMPLAIN HANDLING
            $data['js'] = 'js_ch';
            $data['css'] = 'css_ch';
            $data['content'] = 'dashboard_ch';
            $data['kolom'] = array("No.", "DAERAH", "SPBU 1", "QUOTA 1", "SPBU 2", "QUOTA 2");
            $data['jmlkolom'] = count($data['kolom']);
            $data['rowdata'] = $this->Data_model->jalankanQuery("SELECT * FROM vspbu", 3);
            $data['antrian'] = $this->Data_model->getTotalData('t_antrian', array('ch' => $_SESSION['kode']));
            $data['antri'] = $this->Data_model->getTotalData('t_antrian', array('status' => 'ANTRI'));
            $data['success'] = $this->Data_model->getTotalData('t_antrian', array('status' => 'SUCCESS'));
            $data['cancel'] = $this->Data_model->getTotalData('t_antrian', array('status' => 'CANCEL'));

        } else if ($_SESSION['role'] == '3') {
            // ADMIN
            $data['js'] = 'js_admin';
            $data['css'] = 'css_admin';
            $data['content'] = 'dashboard_admin';
            $data['countdata'] = $this->Data_model->jalankanQuery("SELECT COUNT(*) as jumlah FROM m_spbu", 3);
            $data['rowdata'] = $this->Data_model->jalankanQuery("SELECT * FROM vspbu", 3);
        } else {
            // MANAGEMENT
            $data['js'] = 'js_mana';
            $data['css'] = 'css_mana';
            $data['content'] = 'dashboard_mana';
            $data['kolom'] = array("No.", "SPBU", "QUOTA");
            $data['jmlkolom'] = count($data['kolom']);
            $data['order'] = $this->Data_model->jalankanQuery("SELECT * FROM vorder", 3);
            $this->db->query("SET @rank = 0");
            $data['topagent'] = $this->Data_model->jalankanQuery("SELECT *,@rank := @rank + 1 AS rank FROM vtopagent ORDER BY rank ASC LIMIT 10", 3);
            $this->db->query("SET @rank = 0");
            $data['topspbu'] = $this->Data_model->jalankanQuery("SELECT *,@rank := @rank + 1 AS rank FROM vtopspbu ORDER BY rank ASC LIMIT 10", 3);
            $data['label'] = $this->Data_model->jalankanQuery("SELECT *, SUM(IF(m_jenis.`tipe` = 'BENSIN', t_antrian_detail.`jumlah` * 10, t_antrian_detail.jumlah)) AS jml FROM t_antrian_detail JOIN m_jenis ON m_jenis.`kode` = t_antrian_detail.`jenis` GROUP BY m_jenis.nama_jenis ORDER BY m_jenis.nama_jenis ASC", 3);
            $data['sla'] = $this->Data_model->jalankanQuery("SELECT * FROM sla_spbu", 3);
        }
        $this->load->view('default', $data);
    }

    function getDataSPBU()
    {
        if (IS_AJAX) {
            $sTablex = "";
            // $sTable = 'm_spbu'; // m_barang
            if ($_SESSION['role'] == '1' || $_SESSION['role'] == '2') {
                $sTable = 'vspbu';
                $aColumns = array("kode", "kec", "spbu1", "quota1", "spbu2", "quota2");
                $kolom = "kode,kec,spbu1,quota1,spbu2,quota2";
            } else {
                $sTable = 'm_spbu';
                $aColumns = array("kode", "nama", "quota");
                $kolom = "kode,nama,quota";
            }
            $where = " 1=1";
            $sIndexColumn = "kode";
            if (isset($kolom) && strlen($kolom) > 0) {
                $tQuery = "SELECT $kolom "
                    . "FROM $sTable a $sTablex WHERE $where ";
                echo $this->libglobal->pagingData($aColumns, $sIndexColumn, $sTable, $tQuery, $sTablex);
            }
        } else {
            echo "";
        }
    }
}
