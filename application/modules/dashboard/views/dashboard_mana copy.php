<input type="hidden" id="kolom" value="<?php echo $jmlkolom; ?>">
<!-- Begin Page Content -->
<div class="container-fluid">
    <!-- Content Row -->
    <div class="row">
        <!-- Earnings (Monthly) Card Example -->
        <div class="col-xl-12 col-md-12 mb-4  animated--grow-in">
            <div class="card shadow h-100 py-2">
                <div class="card-body">
                    <div class="row">
                        <!-- Content Column -->
                        <div class="col-lg-4 mb-4">
                            <!-- SPBU Card Example -->
                            <div class="card shadow mb-4">
                                <div class="card-header py-3">
                                    <h6 class="m-0 font-weight-bold text-primary text-center">SPBU QUOTA</h6>
                                </div>
                                <div class="card-body">
                                    <table id="data-spbu" class="table table-hover table-striped">
                                        <thead>
                                            <?php
                                            if ($kolom) {
                                                foreach ($kolom as $key => $value) {
                                                    if (strlen($value) == 0) {
                                                        echo '<th data-type="numeric" class="text-center"></th>';
                                                    } else {
                                                        echo '<th data-column-id="' . $key . '" data-type="numeric" class="text-center">' . $value . '</th>';
                                                    }
                                                }
                                            }
                                            ?>
                                        </thead>
                                        <tbody>
                                        </tbody>
                                    </table>
                                </div>
                            </div>
                        </div>
                        <div class="col-lg-8 mb-8">
                            <div class="row">
                                <div class="col-lg-12">
                                    <div class="card shadow mb-4">
                                        <div class="card-header py-3">
                                            <h6 class="m-0 font-weight-bold text-primary text-center">ORDER ITEM</h6>
                                        </div>
                                        <div class="card-body">
                                            <canvas id="myChart"></canvas>
                                        </div>
                                    </div>
                                </div>
                                <div class="col-lg-12">
                                    <!-- CUSTOMER Card Example -->
                                    <div class="card shadow mb-4">
                                        <div class="card-header py-3">
                                            <h6 class="m-0 font-weight-bold text-primary text-center">CUSTOMER</h6>
                                        </div>
                                        <div class="card-body">
                                            <table id="data-order" class="table table-hover">
                                                <thead>
                                                    <th class="text-center head odd">Jumlah Order</th>
                                                    <th class="text-center head odd">Success Order</th>
                                                    <th class="text-center head odd">Antrian Order</th>
                                                </thead>
                                                <tbody>
                                                    <tr>
                                                        <td class="text-center"><?= $order[0]->totaldata; ?></td>
                                                        <td class="text-center"><?= $order[0]->success; ?></td>
                                                        <td class="text-center"><?= $order[0]->antri; ?></td>
                                                    </tr>
                                                </tbody>
                                                <tfoot>
                                                    <th class="text-center foot odd">Total Bensin Dikeluarkan</th>
                                                    <th class="text-center foot odd"></th>
                                                    <th class="text-center foot odd">Total Gas Dikeluarkan</th>
                                                    <tr>
                                                        <td class="text-center"><?= $order[0]->bensin . ' Liter'; ?></td>
                                                        <td class="text-center"></td>
                                                        <td class="text-center"><?= $order[0]->gas . ' Tabung'; ?></td>
                                                    </tr>
                                                </tfoot>
                                            </table>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-lg-6 mb-6">
                                    <div class="card shadow mb-4">
                                        <div class="card-header py-3">
                                            <h6 class="m-0 font-weight-bold text-primary text-center">TOP 10 SPBU</h6>
                                        </div>
                                        <div class="card-body">
                                            <canvas id="chartspbu"></canvas>
                                            <!-- <table id="data-top-spbu" class="table table-hover">
                                                <thead>
                                                    <th class=" head odd">SPBU</th>
                                                    <th class="text-center head odd">RANK</th>
                                                </thead>
                                                <tbody>
                                                    <?php //foreach ($topspbu as $row) : 
                                                    ?>
                                                        <tr>
                                                            <td class=""><?php //$row->nama_spbu; 
                                                                            ?></td>
                                                            <td class="text-center"><?php //$row->jumlah; 
                                                                                    ?></td>
                                                        </tr>
                                                    <?php //endforeach; 
                                                    ?>
                                                </tbody>
                                            </table> -->
                                        </div>
                                    </div>
                                </div>
                                <div class="col-lg-6 mb-6">
                                    <div class="card shadow mb-4">
                                        <div class="card-header py-3">
                                            <h6 class="m-0 font-weight-bold text-primary text-center">TOP 10 AGENT</h6>
                                        </div>
                                        <div class="card-body">
                                            <table id="data-top-spbu" class="table table-hover">
                                                <thead>
                                                    <th class=" head odd">Nama Lengkap</th>
                                                    <th class="text-center head odd">RANK</th>
                                                </thead>
                                                <tbody>
                                                    <?php foreach ($topagent as $row) : ?>
                                                        <tr>
                                                            <td class=""><?= $row->nama_lengkap; ?></td>
                                                            <td class="text-center"><?= $row->jumlah; ?></td>
                                                        </tr>
                                                    <?php endforeach; ?>
                                                </tbody>
                                            </table>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <!-- Earnings (Monthly) Card Example -->
    </div>
</div>
<!-- /.container-fluid -->
<div class="row">
                            <div class="col-lg-3 mb-4">
                                <div class="card bg-primary text-white shadow">
                                    <div class="card-body">
                                        ORDER &nbsp;&nbsp;&nbsp;<span class="text-white large"><?= $order[0]->totaldata; ?></span>
                                    </div>
                                </div>
                            </div>
                            <div class="col-lg-3 mb-4">
                                <div class="card bg-success text-white shadow">
                                    <div class="card-body">
                                        SUCCESS &nbsp;&nbsp;&nbsp;<span class="text-white large"><?= $order[0]->success; ?></span>
                                    </div>
                                </div>
                            </div>
                            <div class="col-lg-3 mb-4">
                                <div class="card bg-warning text-white shadow">
                                    <div class="card-body">
                                        WAITING &nbsp;&nbsp;&nbsp;<span class="text-white large"><?= $order[0]->antri; ?></span>
                                    </div>
                                </div>
                            </div>
                            <div class="col-lg-3 mb-4">
                                <div class="card bg-warning text-white shadow">
                                    <div class="card-body">
                                        BO &nbsp;&nbsp;&nbsp;<span class="text-white large"><?= $order[0]->bensin; ?></span>
                                    </div>
                                </div>
                            </div>
                            <div class="col-lg-3 mb-4">
                                <div class="card bg-warning text-white shadow">
                                    <div class="card-body">
                                        GO &nbsp;&nbsp;&nbsp;<span class="text-white large"><?= $order[0]->gas; ?></span>
                                    </div>
                                </div>
                            </div>
                            <!-- <div class="col-lg-6 mb-6"> -->
                            <!-- TOP 10 SPBU CHART -->
                            <!-- <div class="card shadow mb-4"> -->
                            <!-- <div class="card-header py-3"> -->
                            <!-- <h6 class="m-0 font-weight-bold text-primary text-center">TOP 10 SPBU</h6> -->
                            <!-- </div> -->
                            <!-- <div class="card-body"> -->
                            <!-- <canvas id="chartspbu"></canvas> -->
                            <!-- </div> -->
                            <!-- </div> -->
                            <!-- </div> -->

                        </div>