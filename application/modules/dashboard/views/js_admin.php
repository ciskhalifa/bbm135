<script src="<?= base_url('assets/admin') ?>/vendor/datatables/jquery.dataTables.min.js"></script>
<script src="<?= base_url('assets/admin') ?>/vendor/datatables/dataTables.bootstrap4.min.js"></script>
<script src="<?= base_url('assets/admin') ?>/js/demo/datatables-demo.js"></script>
<script>
    $(function() {
        $('#data-table-basic').dataTable();
    });
</script>