    <!-- Begin Page Content -->
    <div class="container-fluid">

        <!-- Page Heading -->
        <div class="d-sm-flex align-items-center justify-content-between mb-4">
            <h1 class="h3 mb-0 text-gray-800">Dashboard</h1>
        </div>

        <!-- Content Row -->
        <div class="row">

            <!-- Earnings (Monthly) Card Example -->
            <div class="col-xl-12 col-md-12 mb-4  animated--grow-in">
                <div class="card shadow h-100 py-2">
                    <div class="card-body">
                        <table id="data-table-basic" class="table table-hover table-striped">
                            <thead>
                                <th>No.</th>
                                <th>Provinsi</th>
                                <th>Kota / Kabupaten</th>
                                <th>Kecamatan</th>
                                <th>SPBU OPT 1</th>
                                <th>Q</th>
                                <th>SPBU OPT 2</th>
                                <th>Q</th>
                            </thead>
                            <tbody>
                                <?php foreach ($rowdata as $row) : ?>
                                    <tr>
                                        <td><?= $row->kode; ?></td>
                                        <td><?= $row->provinsi; ?></td>
                                        <td><?= $row->kota; ?></td>
                                        <td><?= $row->kec; ?></td>
                                        <td><?= $row->spbu1;?></td>
                                        <td><?= $row->quota1;?></td>
                                        <td><?= $row->spbu2; ?></td>
                                        <td><?= $row->quota2; ?></td>
                                    </tr>
                                <?php endforeach; ?>
                            </tbody>

                        </table>
                    </div>
                </div>
            </div>

            <!-- Earnings (Monthly) Card Example -->
        </div>
    </div>
    <!-- /.container-fluid -->