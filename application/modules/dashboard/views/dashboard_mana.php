<input type="hidden" id="kolom" value="<?php echo $jmlkolom; ?>">
<!-- Begin Page Content -->
<div class="container-fluid">
    <!-- Content Row -->
    <div class="row">
        <!-- Earnings (Monthly) Card Example -->
        <div class="col-xl-12 col-md-12 mb-4  animated--grow-in">
            <div class="card shadow h-100 py-2">
                <div class="card-body">
                    <div class="row">
                        <!-- Content Column -->
                        <div class="col-lg-4 mb-4">
                            <!-- SPBU Card Example -->
                            <div class="card shadow mb-4">
                                <div class="card-header py-3">
                                    <h6 class="m-0 font-weight-bold text-primary text-center">SPBU QUOTA</h6>
                                </div>
                                <div class="card-body">
                                    <table id="data-spbu" class="table table-hover table-striped">
                                        <thead>
                                            <?php
                                            if ($kolom) {
                                                foreach ($kolom as $key => $value) {
                                                    if (strlen($value) == 0) {
                                                        echo '<th data-type="numeric" class=""></th>';
                                                    } else {
                                                        echo '<th data-column-id="' . $key . '" data-type="numeric" class="">' . $value . '</th>';
                                                    }
                                                }
                                            }
                                            ?>
                                        </thead>
                                        <tbody>
                                        </tbody>
                                    </table>
                                </div>
                                <div class="col-lg-12">
                                    <!-- TOP 10 SPBU CHART -->
                                    <div class="card shadow mb-4">
                                        <div class="card-header py-3">
                                            <h6 class="m-0 font-weight-bold text-primary text-center">TOP 10 SPBU</h6>
                                        </div>
                                        <div class="card-body">
                                            <table id="data-top-spbu" class="table table-hover">
                                                <thead>
                                                    <th class=" head odd">SPBU</th>
                                                    <th class="text-center head odd">ORDER</th>
                                                    <th class="text-center head odd">RANK</th>
                                                </thead>
                                                <tbody>
                                                    <?php foreach ($topspbu as $row) : ?>
                                                        <tr>
                                                            <td class=""><?= $row->nama_spbu; ?></td>
                                                            <td class="text-center"><?= $row->jumlah; ?></td>
                                                            <td class="text-center"><?= $row->rank; ?></td>
                                                        </tr>
                                                    <?php endforeach; ?>
                                                </tbody>
                                            </table>
                                        </div>
                                    </div>
                                </div>
                                <div class="col-lg-12">
                                    <div class="card shadow mb-4">
                                        <div class="card-header py-3">
                                            <h6 class="m-0 font-weight-bold text-primary text-center">TOP 10 AGENT</h6>
                                        </div>
                                        <div class="card-body">
                                            <table id="data-top-spbu" class="table table-hover">
                                                <thead>
                                                    <th class=" head odd">FULLNAME</th>
                                                    <th class="text-center head odd">ORDER</th>
                                                    <th class="text-center head odd">RANK</th>
                                                </thead>
                                                <tbody>
                                                    <?php foreach ($topagent as $row) : ?>
                                                        <tr>
                                                            <td class=""><?= $row->nama_lengkap; ?></td>
                                                            <td class="text-center"><?= $row->jumlah; ?></td>
                                                            <td class="text-center"><?= $row->rank; ?></td>
                                                        </tr>
                                                    <?php endforeach; ?>
                                                </tbody>
                                            </table>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>

                        <div class="col-lg-8">
                            <div class="row">
                                <div class="col-lg-12">
                                    <!-- ORDER ITEM CHART -->
                                    <div class="card shadow mb-4">
                                        <div class="card-header py-3">
                                            <h6 class="m-0 font-weight-bold text-primary text-center">ORDER ITEM</h6>
                                        </div>
                                        <div class="card-body">
                                            <canvas id="myChart"></canvas>
                                        </div>
                                    </div>
                                </div>
                                <div class="col-lg-12">
                                    <div class="row">
                                        <div class="col-xl-4 col-md-6 mb-4">
                                            <div class="card bg-primary shadow">
                                                <div class="card-body">
                                                    <div class="row no-gutters align-items-center">
                                                        <div class="col mr-2">
                                                            <div class="text-xxlarge font-weight-bold text-center text-white text-uppercase mb-1">ORDER</div>
                                                            <div class="h4 mb-0 font-weight-bold text-center text-white"><?= $order[0]->totaldata; ?></div>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="col-xl-2 col-md-6 mb-4">
                                            <div class="card bg-warning shadow">
                                                <div class="card-body">
                                                    <div class="row no-gutters align-items-center">
                                                        <div class="col mr-2">
                                                            <div class="text-xxlarge font-weight-bold text-center text-white text-uppercase mb-1">WAITING</div>
                                                            <div class="h4 mb-0 font-weight-bold text-center text-white"><?= $order[0]->antri; ?></div>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="col-xl-2 col-md-6 mb-4">
                                            <div class="card bg-info shadow">
                                                <div class="card-body">
                                                    <div class="row no-gutters align-items-center">
                                                        <div class="col mr-2">
                                                            <div class="text-xxlarge font-weight-bold text-center text-white text-uppercase mb-1">ON PROGRESS</div>
                                                            <div class="h4 mb-0 font-weight-bold text-center text-white"><?= $order[0]->progress; ?></div>
                                                        </div>

                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="col-xl-2 col-md-6 mb-4">
                                            <div class="card bg-success shadow">
                                                <div class="card-body">
                                                    <div class="row no-gutters align-items-center">
                                                        <div class="col mr-2">
                                                            <div class="text-xxlarge font-weight-bold text-center text-white text-uppercase mb-1">SUCCESS</div>
                                                            <div class="h4 mb-0 font-weight-bold text-center text-white"><?= $order[0]->success; ?></div>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="col-xl-2 col-md-6 mb-4">
                                            <div class="card bg-danger shadow">
                                                <div class="card-body">
                                                    <div class="row no-gutters align-items-center">
                                                        <div class="col mr-2">
                                                            <div class="text-xxlarge font-weight-bold text-center text-white text-uppercase mb-1">CANCEL</div>
                                                            <div class="h4 mb-0 font-weight-bold text-center text-white"><?= $order[0]->cancel; ?></div>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>


                                        <div class="col-lg-6 mb-2">
                                            <div class="card bg-info text-white shadow">
                                                <div class="card-body">
                                                    <div class="row no-gutters align-items-center">
                                                        <div class="col mr-2">
                                                            <div class="text-xxlarge font-weight-bold text-center text-white text-uppercase mb-1">BBM ORDER</div>
                                                            <div class="h4 mb-0 font-weight-bold text-center text-white"><?= $order[0]->bensin . ' Liter'; ?></div>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="col-lg-6 mb-2">
                                            <div class="card bg-info text-white shadow">
                                                <div class="card-body">
                                                    <div class="row no-gutters align-items-center">
                                                        <div class="col mr-2">
                                                            <div class="text-xxlarge font-weight-bold text-center text-white text-uppercase mb-1">GAS ORDER</div>
                                                            <div class="h4 mb-0 font-weight-bold text-center text-white"><?= $order[0]->gas . ' Tabung'; ?></div>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="col-lg-12 mt-4">
                                            <div class="card shadow">
                                                <div class="card-header py-3">
                                                    <h6 class="m-0 font-weight-bold text-primary text-center">SLA SPBU</h6>
                                                </div>
                                                <div class="card-body">
                                                    <table id="data-sla" class="table table-hover table-striped">
                                                        <thead>
                                                            <th>SPBU</th>
                                                            <th>CASE IN</th>
                                                            <th>RESPONSE</th>
                                                            <th>CLOSE</th>
                                                        </thead>
                                                        <tbody>
                                                            <?php foreach ($sla as $row) : ?>
                                                                <tr>
                                                                    <td class=""><?= $row->nama; ?></td>
                                                                    <td class=""><?= $row->agent_to_ch; ?></td>
                                                                    <td class=""><?= $row->ch_to_spbu; ?></td>
                                                                    <td class=""><?= $row->spbu_to_cust; ?></td>
                                                                </tr>
                                                            <?php endforeach; ?>
                                                        </tbody>
                                                    </table>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>

                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
<!-- Earnings (Monthly) Card Example -->
</div>
</div>
<!-- /.container-fluid -->