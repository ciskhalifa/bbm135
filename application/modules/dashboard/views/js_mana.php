<script src="<?= base_url('assets/admin') ?>/vendor/datatables/jquery.dataTables.min.js"></script>
<script src="<?= base_url('assets/admin') ?>/vendor/datatables/dataTables.bootstrap4.min.js"></script>
<script src="<?= base_url('assets/admin') ?>/js/demo/datatables-demo.js"></script>
<script src="https://cdn.jsdelivr.net/npm/chart.js@2.8.0"></script>
<!-- <script src="https://cdn.jsdelivr.net/gh/emn178/chartjs-plugin-labels/src/chartjs-plugin-labels.js"></script> -->
<script src="https://cdn.jsdelivr.net/npm/chartjs-plugin-datalabels@0.7.0"></script>

<script>
    $(function() {
        // CHART ORDER ITEM
        var options = {
            legend: {
                position: 'right',
                display: false,
                fontSize: 9,
                padding: 10,
                boxWidth: 2
            },
            responsive: true,
            maintainAspectRatio: true,
            tooltips: {
                enabled: true,
                mode: 'nearest'
            },

            plugins: {
                datalabels: {
                    align: 'end',
                    anchor: 'top',
                    borderRadius: 25,
                    borderWidth: 2,
                    color: function(ctx){
                        return 'white';
                    },
                    font: function(context) {
                        var w = context.chart.width;
                        return {
                            size: w < 512 ? 12 : 14,
                            weight: 'bold'
                        };
                    },
                    formatter: function(value, context) {
                        return context.dataset.data[context.dataIndex]+ '% ' + context.chart.data.labels[context.dataIndex];
                    }
                }
                // labels: {
                //     render: 'percentage',
                //     fontColor: '#fff',
                //     fontSize: 14,
                //     fontStyle: 'bold',
                //     precision: 0
                // }
            },
        };
        var ctx = document.getElementById('myChart').getContext('2d');
        var chart = new Chart(ctx, {
            // The type of chart we want to create
            type: 'pie',
            // The data for our dataset
            data: {
                labels: [<?php for ($i = 0; $i < count($label); $i++) {
                                echo '"' . $label[$i]->nama_jenis . '",';
                            } ?>],
                datasets: [{
                    label: 'ORDER ITEM',
                    backgroundColor: [
                        '#C9F0AD',
                        '#B2D732',
                        '#EB2815',
                        '#347B98',
                        '#092834',
                        '#E85302'
                    ],
                    borderColor: [
                        '#C9F0AD',
                        '#B2D732',
                        '#EB2815',
                        '#347B98',
                        '#092834',
                        '#E85302'
                    ],
                    data: [<?php for ($i = 0; $i < count($label); $i++) {
                                echo '"' . $label[$i]->jml . '",';
                            } ?>]
                }]
            },

            // Configuration options go here
            options: options
        });
    });
</script>

<script>
    $(function() {
        // CHART RANK SPBU
        var options = {
            responsive: true,
            maintainAspectRatio: false,
            scales: {
                xAxes: [{
                    barPercentage: 0.1,
                    gridLines: {
                        offsetGridLines: true
                    }
                }]
            },
            legend: {
                display: false
            },
            tooltips: {
                titleMarginBottom: 10,
                titleFontColor: '#6e707e',
                titleFontSize: 14,
                backgroundColor: "rgb(255,255,255)",
                bodyFontColor: "#858796",
                borderColor: '#dddfeb',
                borderWidth: 1,
                xPadding: 15,
                yPadding: 15,
                displayColors: false,
                caretPadding: 10,
                callbacks: {
                    label: function(tooltipItem, chart) {
                        var datasetLabel = chart.datasets[tooltipItem.datasetIndex].label || '';
                        console.log(datasetLabel);
                        return datasetLabel + 'Rank : ' + tooltipItem.yLabel;
                    }
                }
            },
            plugins: {
                labels: {
                    render: 'value',
                    fontColor: '#fff',
                    fontSize: 14,
                    fontStyle: 'bold',
                    precision: 0
                }
            },
        };
        var ctx = document.getElementById('chartspbu').getContext('2d');
        var chart = new Chart(ctx, {
            // The type of chart we want to create
            type: 'bar',
            // The data for our dataset
            data: {
                labels: [<?php for ($i = 0; $i < count($topspbu); $i++) {
                                echo '"' . $topspbu[$i]->nama_spbu . '",';
                            } ?>],
                datasets: [{
                    label: '',
                    backgroundColor: '#E85302',
                    borderColor: '#E85302',
                    data: [<?php for ($i = 0; $i < count($topspbu); $i++) {
                                echo '"' . $topspbu[$i]->jumlah . '",';
                            } ?>]
                }]
            },

            // Configuration options go here
            options: options
        });
    });
</script>

<script>
    $(function() {
        var myTable;
        if ($("#kolom").val() > 0) {
            myTable = $('#data-spbu').dataTable({
                dom: "<'row'<'col-md-5'l><'col-md-7'f>r<'clear'>>t<'row'<'col-md-6'i><'col-md-6'p>>",
                bProcessing: true,
                bServerSide: true,
                retrieve: true,
                responsive: false,
                lengthChange: false,
                searching: false,
                autoWidth: false,
                info: false,
                oLanguage: {
                    sLoadingRecords: "Tunggu sejenak - memuat...",
                    sProcessing: '<div style="text-align:center;">Sedang Proses</div>',
                    oPaginate: {
                        sFirst: "<<",
                        sLast: ">>",
                        sNext: ">",
                        sPrevious: "<"
                    }
                },
                sAjaxSource: '<?= base_url('dashboard/getDataSPBU') ?>',
                fnServerData: function(sSource, aoData, fnCallback, oSettings) {
                    oSettings.jqXHR = $.ajax({
                        dataType: "json",
                        type: "POST",
                        url: sSource,
                        data: aoData,
                        success: fnCallback
                    })
                },
                aoColumnDefs: [{
                    aTargets: [2],
                    sClass: "center p-r-0",
                    sWidth: "20px"
                }],
            });
            setInterval(function() {
                myTable.api().ajax.reload();
            }, 300000);
        }
    });
</script>