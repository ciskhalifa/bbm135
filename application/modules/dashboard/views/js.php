<script src="<?= base_url('assets/admin') ?>/vendor/datatables/jquery.dataTables.min.js"></script>
<script src="<?= base_url('assets/admin') ?>/vendor/datatables/dataTables.bootstrap4.min.js"></script>
<script src="<?= base_url('assets/admin') ?>/js/demo/datatables-demo.js"></script>
<script>
    $(function() {
        $('#data-table-basic').dataTable();
    });
</script>

<script>
    $(function() {
        var myTable;
        if ($("#kolom").val() > 0) {
            myTable = $('#data-spbu').dataTable({
                dom: "<'row'<'col-md-5'l><'col-md-7'f>r<'clear'>>t<'row'<'col-md-6'i><'col-md-6'p>>",
                bProcessing: true,
                bServerSide: true,
                retrieve: true,
                responsive: false,
                lengthChange: true,
                searching: true,
                info: true,
                oLanguage: {
                    sLoadingRecords: "Tunggu sejenak - memuat...",
                    sProcessing: '<div style="text-align:center;">Sedang Proses</div>',
                    oPaginate: {
                        sFirst: "<<",
                        sLast: ">>",
                        sNext: ">",
                        sPrevious: "<"
                    }
                },
                sAjaxSource: '<?= base_url('dashboard/getDataSPBU') ?>',
                fnServerData: function(sSource, aoData, fnCallback, oSettings) {
                    oSettings.jqXHR = $.ajax({
                        dataType: "json",
                        type: "POST",
                        url: sSource,
                        data: aoData,
                        success: fnCallback
                    })
                },
               
            });
            setInterval(function() {
                myTable.api().ajax.reload();
            }, 300000);
        }
    });
</script>