<div>
    <a href="javascript:;" id="back" class="btn btn-primary btn-md">
        <span class="m-0 font-weight-bold text-default text-center">BACK</span>
    </a>
</div>
<br>
<div class="row">
    <div class="col-md-6">
        <div class="card shadow mb-2 animated--grow-in">
            <div class="card-header py-3">
                <h6 class="m-0 font-weight-bold text-primary text-center">Customer Profile</h6>
            </div>
            <div class="card-body">
                <div class="form-group row">
                    <label class="col-md-4 label-control">No Antrian</label>
                    <div class="col-md-8">
                        <input type="text" class="form-control input-sm" placeholder="No Antrian" name="no_antrian" id="no_antrian" value="<?= $rowdata->kode_antri; ?>" data-error="wajib diisi" required readonly>
                        <div class="help-block with-errors"></div>
                    </div>
                </div>
                <?php if ($rowdata->jenis_pesanan == 'GAS'):?>
                <div class="form-group row">
                    <label class="col-md-4 label-control">NIK</label>
                    <div class="col-md-8">
                        <input type="text" class="form-control input-sm" placeholder="Nama Pemesan" name="nama_pemesan" id="nama_pemesan" value="<?= $rowdata->nik; ?>" data-error="wajib diisi" required readonly>
                        <div class="help-block with-errors"></div>
                    </div>
                </div>
                <?php endif; ?>
                <div class="form-group row">
                    <label class="col-md-4 label-control">Nama Pemesan</label>
                    <div class="col-md-8">
                        <input type="text" class="form-control input-sm" placeholder="Nama Pemesan" name="nama_pemesan" id="nama_pemesan" value="<?= $rowdata->nama_pemesan; ?>" data-error="wajib diisi" required readonly>
                        <div class="help-block with-errors"></div>
                    </div>
                </div>
                <div class="form-group row">
                    <label class="col-md-4 label-control">HP</label>
                    <div class="col-md-8">
                        <input type="number" class="form-control input-sm" placeholder="No HP" name="no_hp" id="no_hp" value="<?= $rowdata->no_hp; ?>" data-error="wajib diisi" required readonly>
                        <div class="help-block with-errors"></div>
                    </div>
                </div>
                <div class="form-group row">
                    <label class="col-md-4 label-control">No Yang Menghubungi</label>
                    <div class="col-md-8">
                        <input type="number" class="form-control input-sm" placeholder="No Lainnya" name="no_lainnya" id="no_lainnya" value="<?= $rowdata->no_lainnya; ?>" readonly>
                        <div class="help-block with-errors"></div>
                    </div>
                </div>
                <div class="form-group row">
                    <label class="col-md-4 label-control">Alamat Lengkap</label>
                    <div class="col-md-8">
                        <textarea class="form-control input-sm" placeholder="Alamat Lengkap" name="alamat" id="alamat" readonly><?= $rowdata->alamat; ?></textarea>
                        <div class="help-block with-errors"></div>
                    </div>
                </div>
                <div class="form-group row">
                    <label class="col-md-4 label-control">Kota</label>
                    <div class="col-md-8">
                        <input type="text" class="form-control input-sm" placeholder="Kota" name="no_antrian" id="no_antrian" value="<?= $rowdata->nama_kota; ?>" data-error="wajib diisi" required readonly>
                    </div>
                </div>
                <div class="form-group row">
                    <label class="col-md-4 label-control">Kecamatan</label>
                    <div class="col-md-8">
                        <input type="text" class="form-control input-sm" placeholder="Kecamatan" name="no_antrian" id="no_antrian" value="<?= $rowdata->kec; ?>" data-error="wajib diisi" required readonly>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div class="col-md-6">
        <div class="card shadow mb-2 animated--grow-in">
            <div class="card-header py-3">
                <h6 class="m-0 font-weight-bold text-primary text-center">Detail Pesanan</h6>
            </div>
            <div class="card-body">
                <div class="row">
                    <div class="col-md-12">
                        <?php if ($rowdata->jenis_pesanan == 'GAS'): ?>
                        <div class="form-group row">
                            <label class="col-md-4 label-control">Agen GAS</label>
                            <div class="col-md-8">
                                <input type="text" class="form-control input-sm" placeholder="AGEN" name="no_antrian" id="no_antrian" value="<?= $rowdata->nama_agen; ?>" data-error="wajib diisi" required readonly>
                            </div>
                        </div>
                        <?php else: ?>
                        <div class="form-group row">
                            <label class="col-md-4 label-control">SPBU</label>
                            <div class="col-md-8">
                                <input type="text" class="form-control input-sm" placeholder="SPBU" name="no_antrian" id="no_antrian" value="<?= $rowdata->nama_spbu; ?>" data-error="wajib diisi" required readonly>
                            </div>
                        </div>
                        <?php endif; ?>
                        <div class="form-group row">
                            <label class="col-md-4 label-control">Status Order</label>
                            <div class="col-md-8">
                                <input type="text" class="form-control input-sm" placeholder="status" name="status" id="status" value="<?= $rowdata->status; ?>" data-error="wajib diisi" required readonly>
                                <div class="help-block with-errors"></div>
                            </div>
                        </div>
                        <div class="form-group row">
                            <label class="col-md-4 label-control">Waktu Kirim</label>
                            <div class="col-md-8">
                                <input type="text" class="form-control input-sm" placeholder="Waktu Kirim" name="tgl_data" id="tgl_data" value="<?= date('d F Y H:i:s', strtotime($rowdata->waktu_kirim)); ?>" data-error="wajib diisi" required readonly>
                                <div class="help-block with-errors"></div>
                            </div>
                        </div>
                        <div class="form-group row">
                            <label class="col-md-4 label-control">Tanggal Data</label>
                            <div class="col-md-8">
                                <input type="text" class="form-control input-sm" placeholder="Tanggal Data" name="tgl_data" id="tgl_data" value="<?= date('d F Y H:i:s', strtotime($rowdata->tgl_data)); ?>" data-error="wajib diisi" required readonly>
                                <div class="help-block with-errors"></div>
                            </div>
                        </div>
                        <div class="form-group row">
                            <label class="col-md-4 label-control">Catatan</label>
                            <div class="col-md-8">
                                <textarea class="form-control input-sm" placeholder="Catatan" name="alamat" id="alamat" readonly><?= $rowdata->catatan; ?></textarea>
                                <div class="help-block with-errors"></div>
                            </div>
                        </div>
                    </div>
                    <div class="col-md-12">
                        <table class="table table-striped" style="width:100%">
                            <thead>
                                <tr>
                                    <th>#</th>
                                    <th>Item</th>
                                    <th>Jumlah</th>
                                    <th>Subtotal</th>
                            </thead>
                            <tbody>
                                <?php $i = 1;
                                foreach ($rowdetail as $row) : ?>
                                    <tr>
                                        <td><?= $i++; ?></td>
                                        <td><?= $row->nama_jenis; ?></td>
                                        <td><?= $row->jumlah; ?></td>
                                        <td><?= "Rp. " . number_format($row->subtotal, 0, ',', '.'); ?></td>
                                    </tr>
                                <?php endforeach; ?>
                            </tbody>
                            <tfoot class="tfoot-b">
                                <tr>
                                    <td>Total</td>
                                    <td></td>
                                    <td></td>
                                    <td><?= "Rp. " . number_format($total[0]->total, 0, ',', '.'); ?></td>
                                </tr>
                            </tfoot>
                        </table>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>

<script>
    $("#back").on("click", function() {
        $("#tabelpegawai").fadeIn('fast');
        $("#containerform").fadeOut();
        $("#contentdetail").html("");
    });
</script>