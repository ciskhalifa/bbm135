<div class="container-fluid">
    <div id="tabelpegawai" class="slideInDown animated--grow-in" data-appear="appear" data-animation="slideInDown">
        <div class="content-detached">
            <div class="content-body">
                <section class="row">
                    <div class="col-md-12">
                        <div class="card shadow mb-2">
                            <div class="card-header py-3">
                                <h4 class="m-0 font-weight-bold text-primary"> List <?= $this->uri->segment('1'); ?></h4>
                                <a class="heading-elements-toggle"><i class="icon-arrow-right-4"></i></a>
                                <div class="box-tools pull-right">
                                    <a href="javascript:;" class="btn btn-primary btn-sm" id="openform" data-tab="" data-original-title="Tambah Data" data-trigger="hover" data-toggle="tooltip" data-placement="bottom" title="">
                                        <i class="fa fa-plus"></i>
                                    </a>
                                </div>
                            </div>
                            <div class="card-body">
                                <table id="AntrianTable" class="table table-white-space table-bordered ">
                                    <thead>
                                        <tr>
                                            <?php
                                            if ($kolom) {
                                                foreach ($kolom as $key => $value) {
                                                    if (strlen($value) == 0) {
                                                        echo '<th data-type="numeric"></th>';
                                                    } else {
                                                        echo '<th data-column-id="' . $key . '" data-type="numeric">' . $value . '</th>';
                                                    }
                                                }
                                            }
                                            ?>
                                        </tr>
                                    </thead>
                                    <tbody>
                                        <?php foreach ($rowdata as $row) : ?>
                                            <tr>
                                                <td><?= $row->kode_antri; ?></td>
                                                <td><?= $row->nama_pemesan; ?></td>
                                                <td><?= $row->no_hp . ' / ' . $row->no_lainnya; ?></td>
                                                <td><?= $row->nama_kota . ' / ' . $row->kec; ?></td>
                                                <td><?= date('d F Y H:i:s', strtotime($row->waktu_kirim)); ?></td>
                                                <td><?= $row->status; ?></td>
                                                <?php if ($_SESSION['role'] == '1') : ?>
                                                    <td>
                                                        <button type="button" data-kode="<?= $row->kode_antri ?>" class="btn btn-sm btn-info detail" id="detail"><i class="fa fa-eye"></i></button>
                                                    </td>
                                                <?php elseif ($_SESSION['role'] == '2') : ?>
                                                    <td>
                                                        <button type="button" data-kode="<?= $row->kode_antri ?>" class="btn btn-sm btn-info detail" id="detail"><i class="fa fa-eye"></i></button>
                                                        <button type="button" data-kode="<?= $row->kode_antri ?>" data-status="<?= $row->status ?>" class="btn btn-sm btn-warning edit" id="edit"><i class="far fa-edit"></i></button>
                                                        <button type="button" data-kode="<?= $row->kode_antri ?>" class="btn btn-sm btn-danger delete" id="delete"><i class="fa fa-trash"></i></button>
                                                    </td>
                                                <?php endif; ?>
                                            </tr>
                                        <?php endforeach; ?>
                                    </tbody>
                                </table>
                            </div>
                        </div>
                    </div>
                </section>
            </div>
        </div>
    </div>

    <div id="containerdetail" class="content-detached">
        <div class="content-body">
            <div id="contentdetail">
            </div>
        </div>
    </div>

    <div class="modal fade text-xs-left animated--grow-in" id="modalForm" tabindex="-1" role="dialog" aria-labelledby="myModalLabel1" style="display: none;" aria-hidden="true">
        <div class="modal-dialog modal-lg" role="document">
            <div class="modal-content">
                <form role="form" id="sendForm" enctype="multipart/form-data" class="form-horizontal" action="<?= base_url('order/simpanData'); ?>" method="POST">
                    <div class="modal-header">
                        <h4 class="modal-title">Form Order</h4>
                        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                            <span aria-hidden="true">&times;</span>
                        </button>
                    </div>
                    <div class="modal-body">
                        <div class="form-group row">
                            <label class="col-md-4 label-control">No Order</label>
                            <div class="col-md-8">
                                <input type="text" class="form-control input-sm" placeholder="No Order" name="no_antrian" id="no_antrian" value="<?= $no_antrian; ?>" data-error="wajib diisi" required readonly>
                                <div class="help-block with-errors"></div>
                            </div>
                        </div>
                        <div class="form-group row">
                            <label class="col-md-4 label-control">Jenis Pesanan</label>
                            <div class="col-md-8">
                                 <select class="select2 form-control jenis_pesanan" name="jenis_pesanan" id="jenis_pesanan" onchange="" style="width:100%">
                                    <option value="">- Pilihan -</option>
                                    <option value="GAS"> GAS </option>
                                    <option value="PDS"> PDS </option>
                                </select>
                            </div>
                        </div>
                        <div class="form-group row nik">
                            <label class="col-md-4 label-control">NIK</label>
                            <div class="col-md-8">
                                <input type="text" class="form-control input-sm" placeholder="NIK" name="nik" id="nik" value="" data-error="wajib diisi" maxlength="16" required>
                                <div class="help-block with-errors"></div>
                            </div>
                        </div>
                        <div class="form-group row">
                            <label class="col-md-4 label-control">Nama Pemesan</label>
                            <div class="col-md-8">
                                <input type="text" class="form-control input-sm" placeholder="Nama Pemesan" name="nama_pemesan" id="nama_pemesan" value="" data-error="wajib diisi" required>
                                <div class="help-block with-errors"></div>
                            </div>
                        </div>
                        <div class="form-group row">
                            <label class="col-md-4 label-control">HP</label>
                            <div class="col-md-8">
                                <input type="number" class="form-control input-sm" maxlength="13" placeholder="No HP" name="no_hp" id="no_hp" value="" data-error="wajib diisi" required>
                                <div class="help-block with-errors"></div>
                            </div>
                        </div>
                        <div class="form-group row">
                            <label class="col-md-4 label-control">Nomor yang menghubungi</label>
                            <div class="col-md-8">
                                <input type="number" class="form-control input-sm" maxlength="13" placeholder="No Lainnya" name="no_lainnya" id="no_lainnya" value="">
                                <div class="help-block with-errors"></div>
                            </div>
                        </div>
                        <div class="form-group row">
                            <label class="col-md-4 label-control">Alamat</label>
                            <div class="col-md-8">
                                <textarea class="form-control input-sm" placeholder="Alamat Lengkap" name="alamat" id="alamat" value=""></textarea>
                                <div class="help-block with-errors"></div>
                            </div>
                        </div>
                        <div class="form-group row">
                            <label class="col-md-4 label-control">Kota</label>
                            <div class="col-md-8">
                                <select class="select2 form-control kota" name="kota" id="kota" style="width:100%">
                                    <option value="">- Pilihan -</option>
                                    <?php
                                        $n = (isset($arey)) ? $arey['kota'] : '';
                                        $q = $this->Data_model->selectData('m_kota', 'kode');
                                        foreach ($q as $row) {
                                            $kapilih = ($row->kode == $n) ? ' selected=selected' : '';
                                            echo '<option value="' . $row->kode . '"' . $kapilih . '>' . $row->nama . '</option>';
                                        }
                                    ?>
                                </select>
                            </div>
                        </div>
                        <div class="form-group row">
                            <label class="col-md-4 label-control">Kecamatan</label>
                            <div class="col-md-8">
                                <div id="kecArea"></div>
                            </div>
                        </div>

                        <div id="list_agent" style="display: none;">
                            <div class="form-group" id="pesanangas" style="display:block;">
                                <div class="col-label">
                                    <table class="table" id="multipesanangas" style="width: 100%">
                                        <thead>
                                            <tr>
                                                <th style="text-align:center;width:20px">No</th>
                                                <th>Jenis</th>
                                                <th>Pesanan</th>
                                                <th>Jumlah</th>
                                                <th>Harga</th>
                                            </tr>
                                        </thead>
                                        <tbody>
                                            <tr id="row1">
                                                <td style="text-align:center">1</td>
                                                <td>
                                                    <select class="select2 pilihjenisgas form-control" id="selectjenisgas1" name="jenis[]" onchange="loadBarangGas(1)"></select>
                                                </td>
                                                <td id="baranggas1">
                                                </td>
                                                <td id="jumlahgas1">
                                                </td>
                                                <td id="hargagas1">
                                                </td>
                                            </tr>
                                        </tbody>
                                        <tfoot>
                                            <tr>
                                                <td colspan="4">
                                                    <button type="button" class="btn-sm btn btn-danger removepesanangas">
                                                        <i class="fa fa-trash"></i>
                                                    </button>
                                                    <button type="button" class="btn-sm btn btn-primary addpesanangas">
                                                        <i class="fa fa-plus"></i>
                                                    </button>
                                                    <div>
                                                    <p style="font-weight:bold;">Notes: Masukan nominal harga tanpa menggunakan <b style="color:red;">TITIK (.)</b> atau <b style="color:red;">KOMA (,)</b></p>
                                                    </div>
                                                </td>
                                            </tr>
                                        </tfoot>
                                    </table>
                                </div>
                            </div>
                        </div>
                        
                        <div id="list_spbu" style="display: none;">
                            <div class="form-group" id="pesanan" style="display:block;">
                                <div class="col-label">
                                    <table class="table" id="multipesanan" style="width: 100%">
                                        <thead>
                                            <tr>
                                                <th style="text-align:center;width:20px">No</th>
                                                <th>Jenis</th>
                                                <th>Pesanan</th>
                                                <th>Jumlah</th>
                                                <th>Harga</th>
                                            </tr>
                                        </thead>
                                        <tbody>
                                            <tr id="row1">
                                                <td style="text-align:center">1</td>
                                                <td>
                                                    <select class="select2 pilihjenis form-control" id="selectjenis1" name="jenis[]" onchange="loadBarang(1)"></select>
                                                </td>
                                                <td id="barang1">
                                                </td>
                                                <td id="jumlah1">
                                                </td>
                                                <td id="harga1">
                                                </td>
                                            </tr>
                                        </tbody>
                                        <tfoot>
                                            <tr>
                                                <td colspan="4">
                                                    <button type="button" class="btn-sm btn btn-danger removepesanan">
                                                        <i class="fa fa-trash"></i>
                                                    </button>
                                                    <button type="button" class="btn-sm btn btn-primary addpesanan">
                                                        <i class="fa fa-plus"></i>
                                                    </button>
                                                    <div>
                                                    <p style="font-weight:bold;">Notes: Masukan nominal harga tanpa menggunakan <b style="color:red;">TITIK (.)</b> atau <b style="color:red;">KOMA (,)</b></p>
                                                    </div>
                                                </td>
                                            </tr>
                                        </tfoot>
                                    </table>
                                </div>
                            </div>
                        </div>

                        <div class="form-group row">
                            <label class="col-md-4 label-control">Catatan</label>
                            <div class="col-md-8">
                                <textarea class="form-control input-sm" placeholder="Catatan" name="catatan" id="alamat" value=""></textarea>
                                <div class="help-block with-errors"></div>
                            </div>
                        </div>
                        <div class="form-group row" style="overflow:hidden;">
                            <label class="col-md-4 label-control">Tanggal dan Waktu Pengantaran</label>
                            <div class="col-md-8">
                                <input type='text' class="form-control" name="waktu_kirim" id='waktu_kirim' />
                                <div class="help-block with-errors"></div>
                            </div>
                        </div>
                    </div>
                    <div class="modal-footer">
                        <button type="submit" class="btn btn-primary">Simpan</button>
                        <button type="reset" class="btn btn-danger" data-dismiss="modal">Cancel</button>
                    </div>
                </form>
            </div>
        </div>
    </div>
</div>