
<?php

/* Name     : Christiantinus Nesi
 * Email    : christiantinusnesi@gmail.com
 * Created By : Christiantinus Nesi
 */

class Order extends MX_Controller
{

    function __construct()
    {
        parent::__construct();
        if (!isset($_SESSION['username'])) {
            redirect('../login');
        }
        date_default_timezone_set("Asia/Bangkok");
    }

    public function index()
    {
        $data['js'] = 'js';
        $data['css'] = 'css';
        $data['content'] = 'order';
        if ($_SESSION['role'] == '1') {
            $data['rowdata'] = $this->Data_model->jalankanQuery("SELECT * FROM vorder2 WHERE agent=" . $_SESSION['kode'], 3);
        } else if ($_SESSION['role'] == '2') {
            $data['rowdata'] = $this->Data_model->jalankanQuery("SELECT * FROM vorder2 WHERE ch=" . $_SESSION['kode'] . " OR ch IS NULL", 3);
        }
        $data['kolom'] = array("Kode", "Pemesan", "Telepon", "Kota / Kecamatan", "Waktu Pengantaran", "Status", "Opsi");
        $q = $this->Data_model->jalankanQuery("SELECT max(kode_antri) as maxKode FROM t_order", 3);
        $noUrut = (int) substr($q[0]->maxKode, 3, 3);
        $noUrut++;
        $char = "ANT";
        $data['no_antrian'] = $char . sprintf("%03s", $noUrut);
        $this->load->view('default', $data);
    }

    public function simpanData()
    {
        $daerah = $this->input->post('kecamatan');
        $arrdata = array();
        $pesanan = array();

        $arrdata['kode_spbu'] = NULL;
        $arrdata['kode_agen'] = NULL;
        $arrdata['waktu_kirim'] = $this->input->post('waktu_kirim');

        $arrdata['nik'] = $this->input->post('nik');
        $arrdata['jenis_pesanan'] = $this->input->post('jenis_pesanan');
        $arrdata['kode_antri'] = $this->input->post('no_antrian');
        $arrdata['nama_pemesan'] = $this->input->post('nama_pemesan');
        $arrdata['no_hp'] = $this->input->post('no_hp');
        $arrdata['no_lainnya'] = $this->input->post('no_lainnya');
        $arrdata['alamat'] = $this->input->post('alamat');
        $arrdata['agent'] = $_SESSION['kode'];
        $arrdata['kota'] = $this->input->post('kota');
        $arrdata['kecamatan'] = $this->input->post('kecamatan');
        
        if ($this->input->post('barang') && is_array($this->input->post('barang'))) {
            for ($i = 0; $i < count($this->input->post('barang')); $i++) {
                if ($this->input->post('barang') == ''){
                    echo 'TIDAK ADA';
                }else{
                    $q = $this->Data_model->jalankanQuery("SELECT * FROM m_jenis WHERE kode=" . $this->input->post('barang')[$i], 3);
                    $pesanan['kode'] = $arrdata['kode_antri'];
                    $pesanan['jenis'] = $this->input->post('barang')[$i];
                    $pesanan['jumlah'] = $this->input->post('jumlah')[$i];
                    $pesanan['harga'] = $this->input->post('harga')[$i];
                    $pesanan['subtotal'] = $this->input->post('harga')[$i] * $this->input->post('jumlah')[$i];
                    $this->Data_model->simpanData($pesanan, 't_order_detail');
                }
            }
        }
        
        $this->Data_model->simpanData($arrdata, 't_order');
        redirect('order');
    }

    public function view_detail()
    {
        $id = $this->uri->segment(3);
        $kondisi = "kode_antri";
        $data['rowdata'] = $this->Data_model->satuData('vorder2', array($kondisi => $id));
        $data['rowdetail'] = $this->Data_model->jalankanQuery("SELECT t_order_detail.*, m_jenis.nama_jenis FROM  t_order_detail JOIN m_jenis ON m_jenis.kode = t_order_detail.jenis WHERE t_order_detail.kode='$id'", 3);
        $data['total'] = $this->Data_model->jalankanQuery("SELECT SUM(subtotal) as total FROM t_order_detail WHERE kode='$id'", 3);
        $this->load->view('view_detail', $data);
    }

    public function getKecamatan()
    {
        $kec = $this->Data_model->ambilDataWhere('m_kecamatan', array('kode_kota' => $_GET['kode']), 'nama', 'asc');
        echo '<select class="select2 form-control kecamatan" name="kecamatan" id="kecamatan" style="width:100%">' .
            '<option value="">- Pilihan -</option>';
        foreach ($kec as $row) {
            echo '<option value="' . $row->kode . '">' . $row->nama . '</option>';
        }
        echo '</select>';
    }

    public function select_jenis()
    {
        $query = "SELECT DISTINCT(tipe) AS disp FROM m_jenis WHERE tipe='BENSIN'";
        echo json_encode($this->Data_model->jalankanQuery($query, 3));
    }
    public function select_jenis_gas()
    {
        $query = "SELECT DISTINCT(tipe) AS disp FROM m_jenis WHERE tipe='GAS'";
        echo json_encode($this->Data_model->jalankanQuery($query, 3));
    }
    public function getBarang()
    {
        $get = $_GET['tipe'];
        $query = "SELECT kode,nama_jenis AS disp FROM m_jenis WHERE tipe='$get'";
        echo json_encode($this->Data_model->jalankanQuery($query, 3));
    }
}