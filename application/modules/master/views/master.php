    <div class="container-fluid">
        <div class="row">
            <div class="col-md-3 animated--grow-in">
                <div class="card shadow mb-2">
                    <div class="card-header py-3">
                        <h6 class="m-0 font-weight-bold text-primary">Master Data</h6>
                    </div>
                    <div class="card-body">
                        <ul class="list-group">
                            <?php if ($_SESSION['role'] == 3) : ?>
                            <li class="menuitem list-group-item list-group-item-action" data-default="spbu"><a href="javascript:;" class="" style="color:black"><i class="fa fa-cubes"></i> SPBU</a></li>
                            <li class="menuitem list-group-item list-group-item-action" data-default="agen"><a href="javascript:;" class="" style="color:black"><i class="fa fa-cubes"></i> Agen Gas</a></li>
                            <li class="menuitem list-group-item list-group-item-action" data-default="jenis"><a href="javascript:;" class="" style="color:black"><i class="fa fa-cubes"></i> Jenis</a></li>
                            <li class="menuitem list-group-item list-group-item-action" data-default="daerah"><a href="javascript:;" class="" style="color:black"><i class="fa fa-cubes"></i> Daerah</a></li>
                            <li class="menuitem list-group-item list-group-item-action" data-default="user"><a href="javascript:;" class="" style="color:black"><i class="fa fa-user"></i> Users</a></li>
                            <?php endif; ?>
                        </ul>
                    </div>
                </div>
            </div>
            <div id="divhalaman" class="col-md-9">

            </div>
        </div>
    </div>