<?php
/* Name     : Christiantinus Nesi
 * Email    : christiantinusnesi@gmail.com
 * Created By : Christiantinus Nesi
 */
if (isset($rowdata)) {
    $cid = ($aep == 'salin') ? '' : $rowdata->kode;
    $nama_jenis = $rowdata->nama_jenis;
    $satuan = $rowdata->satuan;
    $harga = $rowdata->harga;
    $tipe = $rowdata->tipe;
} else {
    $cid = "";
    $nama_jenis = "";
    $satuan = "";
    $harga = "";
    $tipe ="";
}
?>

<form role="form" id="xfrm" enctype="multipart/form-data" class="form form-horizontal" method="POST">
    <div class="form-body">
        <input type="hidden" name="cid" id="cid" value="<?php echo $cid; ?>">
        <div class="form-group row">
            <label class="col-md-2 label-control">Nama Jenis</label>
            <div class="col-md-6">
                <input type="text" class="form-control input-sm" placeholder="Nama Jenis" name="nama_jenis" id="nama_jenis" value="<?php echo $nama_jenis; ?>" data-error="wajib diisi" required>
                <div class="help-block with-errors"></div>
            </div>
        </div>
        <div class="form-group row">
            <label class="col-md-2 label-control">Tipe</label>
            <div class="col-md-6">
                <select class="select2 form-control spbu" name="tipe" id="type">
                    <option value="">- Pilihan -</option>
                    <?php $n = (isset($tipe)) ? $tipe : ''; ?>
                    <option value="BENSIN" <?= ($n == 'BENSIN') ? " selected= selected" : "" ?>> BENSIN </option>
                    <option value="GAS" <?= ($n == 'GAS') ? " selected= selected" : "" ?>> GAS </option>
                </select>
            </div>
        </div>
        <div class="form-group row">
            <label class="col-md-2 label-control">Satuan</label>
            <div class="col-md-6">
                <input type="text" class="form-control input-sm" placeholder="Satuan" name="satuan" id="satuan" value="<?php echo $satuan; ?>" data-error="wajib diisi" required>
                <div class="help-block with-errors"></div>
            </div>
        </div>
        <div class="form-group row">
            <label class="col-md-2 label-control">Harga</label>
            <div class="col-md-6">
                <input type="text" class="form-control input-sm" placeholder="Harga" name="harga" id="harga" value="<?php echo $harga; ?>" data-error="wajib diisi" required>
                <div class="help-block with-errors"></div>
            </div>
        </div>
        <div class="form-actions">
            <button class="btn btn-primary"><i class="icon-check2"></i> Simpan</button>
            <a href="javascript:" class="btn btn-warning" id="tmblBatal"><i class="icon-cross2"></i> Batal</a>
        </div>
    </div>
</form>
<script>
    $(function() {
        $("#tmblBatal").on("click", function() {
            $("#divdua").slideUp();
            $("#divsatu").slideDown();
            $("#divform").html("");
        });
        $("#xfrm").on("submit", function(c) {
            if (c.isDefaultPrevented()) {} else {
                var b = "master/simpanData/" + $("#tabel").val();
                var a = new FormData($('#xfrm')[0]);
                $.ajax({
                    url: b,
                    type: "POST",
                    data: a,
                    cache: false,
                    contentType: false,
                    processData: false,
                    //dataType: "html",
                    beforeSend: function() {
                        $(".card #divform").isLoading({
                            text: "Proses Simpan",
                            position: "overlay",
                            tpl: ''
                        })
                    },
                    success: function(d) {
                        setTimeout(function() {
                            $(".card #divform").isLoading("hide");
                            myApp.oTable.fnDraw(false);
                            $("#divdua").slideUp();
                            $("#divsatu").slideDown();
                            notify("Penyimpanan berhasil", "success")
                        }, 1000)
                    },
                    error: function() {
                        setTimeout(function() {
                            $(".card #divform").isLoading("hide")
                        }, 1000)
                    }
                });
                return false
            }
            return false
        })
    }); /*]]>*/
</script>