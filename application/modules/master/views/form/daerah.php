<?php
/* Name     : Christiantinus Nesi
 * Email    : christiantinusnesi@gmail.com
 * Created By : Christiantinus Nesi
 */
if (isset($rowdata)) {
    $cid = ($aep == 'salin') ? '' : $rowdata->kode;
    $nama_site = $rowdata->nama_site;
} else {
    $cid = "";
    $nama_site = "";
}
?>

<form role="form" id="xfrm" enctype="multipart/form-data" class="form form-horizontal" method="POST">
    <div class="form-body">
        <input type="hidden" name="cid" id="cid" value="<?php echo $cid; ?>">
        <div class="form-group row">
            <label class="col-md-2 label-control">Provinsi</label>
            <div class="col-md-6">
                <select class="select2 form-control provinsi" name="provinsi" id="provinsi" onchange="loadKabupaten()">
                    <option value="">- Pilihan -</option>
                    <?php
                    $n = (isset($arey)) ? $arey['provinsi'] : '';
                    $q = $this->Data_model->selectData('m_provinsi', 'kode');
                    foreach ($q as $row) {
                        $kapilih = ($row->kode == $n) ? ' selected=selected' : '';
                        echo '<option data-id="' . $row->nama . '" value="' . $row->kode . '" ' . $kapilih . '>' . $row->nama . '</option>';
                    }
                    ?>
                </select>
            </div>
        </div>
        <div class="form-group row">
            <label class="col-md-2 label-control">Kota / Kabupaten</label>
            <div class="col-md-6">
                <div id="kotaArea"></div>
            </div>
        </div>
        <div class="form-group row">
            <label class="col-md-2 label-control">Kecamatan</label>
            <div class="col-md-6">
                <div id="kecamatanArea"></div>
            </div>
        </div>
        <div class="form-group row">
            <label class="col-md-2 label-control">SPBU Opt 1</label>
            <div class="col-md-6">
                <select class="select2 form-control spbu" name="kode_spbu1" id="spbu">
                    <option value="">- Pilihan -</option>
                    <?php
                    $n = (isset($arey)) ? $arey['kode_spbu'] : '';
                    $q = $this->Data_model->selectData('m_spbu', 'kode');
                    foreach ($q as $row) {
                        $kapilih = ($row->kode == $n) ? ' selected=selected' : '';
                        echo '<option data-id="' . $row->nama . '" value="' . $row->kode . '" ' . $kapilih . '>' . $row->nama . '</option>';
                    }
                    ?>
                </select>
            </div>
        </div>
        <div class="form-group row">
            <label class="col-md-2 label-control">SPBU Opt 2</label>
            <div class="col-md-6">
                <select class="select2 form-control spbu" name="kode_spbu2" id="spbu">
                    <option value="">- Pilihan -</option>
                    <?php
                    $n = (isset($arey)) ? $arey['kode_spbu'] : '';
                    $q = $this->Data_model->selectData('m_spbu', 'kode');
                    foreach ($q as $row) {
                        $kapilih = ($row->kode == $n) ? ' selected=selected' : '';
                        echo '<option data-id="' . $row->nama . '" value="' . $row->kode . '" ' . $kapilih . '>' . $row->nama . '</option>';
                    }
                    ?>
                </select>
            </div>
        </div>
        <div class="form-actions">
            <button class="btn btn-primary"><i class="icon-check2"></i> Simpan</button>
            <a href="javascript:" class="btn btn-warning" id="tmblBatal"><i class="icon-cross2"></i> Batal</a>
        </div>
    </div>
</form>
<script>
    $(function() {
        $('.provinsi').select2();

        $("#tmblBatal").on("click", function() {
            $("#divdua").slideUp();
            $("#divsatu").slideDown();
            $("#divform").html("");
        });
        $("#xfrm").on("submit", function(c) {
            if (c.isDefaultPrevented()) {} else {
                var b = "master/simpanData/" + $("#tabel").val();
                var a = new FormData($('#xfrm')[0]);
                $.ajax({
                    url: b,
                    type: "POST",
                    data: a,
                    cache: false,
                    contentType: false,
                    processData: false,
                    //dataType: "html",
                    beforeSend: function() {
                        $(".card #divform").isLoading({
                            text: "Proses Simpan",
                            position: "overlay",
                            tpl: ''
                        })
                    },
                    success: function(d) {
                        setTimeout(function() {
                            $(".card #divform").isLoading("hide");
                            myApp.oTable.fnDraw(false);
                            $("#divdua").slideUp();
                            $("#divsatu").slideDown();
                            notify("Penyimpanan berhasil", "success")
                        }, 1000)
                    },
                    error: function() {
                        setTimeout(function() {
                            $(".card #divform").isLoading("hide")
                        }, 1000)
                    }
                });
                return false
            }
            return false
        })
    }); /*]]>*/
</script>