<?php

/* Name     : Christiantinus Nesi
 * Email    : christiantinusnesi@gmail.com
 * Created By : Christiantinus Nesi
 */

class Master extends MX_Controller
{

    public function __construct()
    {
        parent::__construct();
        if (!isset($_SESSION['username'])) {
            redirect('../login');
        }
    }

    public function index()
    {
        // halaman
        $data['content'] = 'master';
        // js tambahan dan css
        $data['js'] = 'js';
        $data['css'] = 'css';

        $this->load->view('default', $data);
    }

    public function loadHalaman()
    {
        $konten['tabel'] = $this->uri->segment(3); // barang
        switch ($this->uri->segment(3)): 
            case 'daerah':
                $konten['kolom'] = array("Kode", "Provinsi", "Kota / Kabupaten", "Kecamatan", "Opt 1", "Opt 2", "Opsi");
                break;
            case 'spbu':
                $konten['kolom'] = array("Kode", "Nama SPBU", "Opsi");
                break;
            case 'agen':
                $konten['kolom'] = array("Kode", "Nama Agen", "Provinsi", "Kota", "Kecamatan", "Opsi");
                break;
            case 'jenis':
                $konten['kolom'] = array("Kode", "Nama Jenis", "Satuan", "Harga", "Opsi");
                break;
            case 'user':
                $konten['kolom'] = array("Kode", "Nama Lengkap", "Username", "Role", "Opsi");
                break;
            default:
                $konten['kolom'] = "";
                break;
        endswitch;

        $konten['jmlkolom'] = count($konten['kolom']);
        $this->load->view('halaman', $konten);
    }

    public function getData()
    {
        /*
             * list data
             */
        if (IS_AJAX) {
            $sTablex = "";
            $order = "";
            $sTable = 'm_' . $this->uri->segment(3); // m_barang
            $k = '';
            switch ($this->uri->segment(3)): 
                case 'daerah':
                    $aColumns = array("kode", "provinsi", "kota", "kecamatan", "nama_spbu1", "nama_spbu2", "opsi");
                    $kolom = "a.kode,prov.nama as provinsi ,kota.nama as kota,kec.nama as kecamatan,b.nama as nama_spbu1, c.nama as nama_spbu2";
                    $where = " 1=1";
                    $sIndexColumn = "kode";
                    break;
                case 'spbu':
                    $aColumns = array("kode", "nama", "opsi");
                    $kolom = "kode,nama";
                    $where = " 1=1";
                    $sIndexColumn = "kode";
                    break;
                case 'agen':
                    $aColumns = array("kode", "nama_agen", "provinsi", "kota", "kecamatan", "opsi");
                    $kolom = "a.kode,a.nama_agen, prov.nama as provinsi, kota.nama as kota, kec.nama as kecamatan";
                    $where = " 1=1";
                    $sIndexColumn = "kode";
                    break;
                case 'jenis':
                    $aColumns = array("kode", "nama_jenis", "satuan", "harga", "opsi");
                    $kolom = "kode,nama_jenis,satuan,CONCAT('Rp. ',fRupiah(harga)) as harga";
                    $where = " 1=1";
                    $sIndexColumn = "kode";
                    break;
                case 'user':
                    $aColumns = array("kode", "nama_lengkap", "username", "role", "opsi");
                    $kolom = "kode, nama_lengkap, username,CASE WHEN role = '1' THEN 'Agent' WHEN '2' THEN 'Complain Handling' WHEN '3' THEN 'Admin' ELSE '??' END  AS role";
                    $where = "1=1";
                    $sIndexColumn = "kode";
                    break;
                default:

                    break;
            endswitch;
            if (isset($kolom) && strlen($kolom) > 0) {
                //$where = "";
                if ($this->uri->segment(3) == 'agen'){
                        $tQuery = "SELECT $kolom, '$k' AS opsi "
                        . "FROM $sTable a JOIN m_provinsi as prov ON prov.kode = a.provinsi JOIN m_kota as kota ON kota.kode = a.kota JOIN m_kecamatan as kec ON kec.kode = a.kecamatan WHERE $where";
                    echo $this->libglobal->pagingData($aColumns, $sIndexColumn, $sTable, $tQuery, $sTablex);
                }else if ($this->uri->segment(3) == "daerah") {
                    $tQuery = "SELECT $kolom, '$k' AS opsi "
                        . "FROM $sTable a JOIN m_spbu as b ON b.kode = a.kode_spbu1 JOIN m_spbu as c ON c.kode = a.kode_spbu2 JOIN m_provinsi as prov ON prov.kode = a.provinsi JOIN m_kota as kota ON kota.kode = a.kota JOIN m_kecamatan as kec ON kec.kode = a.kecamatan WHERE $where";
                    echo $this->libglobal->pagingData($aColumns, $sIndexColumn, $sTable, $tQuery, $sTablex);
                } else {
                    $tQuery = "SELECT $kolom ,'$k' AS opsi  "
                        . "FROM $sTable a $sTablex WHERE $where ";
                    echo $this->libglobal->pagingData($aColumns, $sIndexColumn, $sTable, $tQuery, $sTablex);
                }
            } else {
                echo "";
            }
        }
    }

    public function loadForm()
    {
        $konten['aep'] = $this->uri->segment(5);
        if ($this->uri->segment(4) != '-') {
            $nilai = $this->uri->segment(4);
            if (strtoupper($this->uri->segment(5)) == 'SALIN') {
                if ($this->uri->segment(6) == '' || strlen(trim($this->uri->segment(6))) > 1) {
                    if (strpos($nilai, '-')) {
                        $exp = explode("-", $nilai);
                        $kondisi = array('kode' => $exp[0], 'seri' => $exp[1]);
                    } else {
                        $kondisi = array('kode' => $nilai);
                    }
                    $tabel = 'm_' . ((strlen(trim($this->uri->segment(6))) > 1) ? $this->uri->segment(3) : $this->uri->segment(3));
                } else {
                    $tabel = 'm_' . ((strlen(trim($this->uri->segment(6))) > 1) ? $this->uri->segment(3) : $this->uri->segment(3) . '_' . strtolower($this->uri->segment(6)));
                    $exp = explode("-", $nilai);
                    $kondisi = array('kode' => $exp[0], 'seri' => $exp[1]);
                }
            } else {
                if ($this->uri->segment(5) == '' || strlen(trim($this->uri->segment(5))) > 1) {
                    if (strpos($nilai, '-')) {
                        $exp = explode("-", $nilai);
                        $kondisi = array('kode' => $exp[0], 'seri' => $exp[1]);
                    } else {
                        $kondisi = array('kode' => $nilai);
                    }
                    $tabel = 'm_' . ((strlen(trim($this->uri->segment(5))) > 1) ? $this->uri->segment(3) : $this->uri->segment(3));
                } else {
                    $tabel = 'm_' . ((strlen(trim($this->uri->segment(5))) > 1) ? $this->uri->segment(3) : $this->uri->segment(3) . '_' . strtolower($this->uri->segment(5)));
                    $exp = explode("-", $nilai);
                    $kondisi = array('kode' => $exp[0], 'seri' => $exp[1]);
                }
            }
            $konten['rowdata'] = $this->Data_model->satuData($tabel, $kondisi);
            if (strtoupper($this->uri->segment(5)) == 'SALIN') {
                $konten['jenis'] = ($this->uri->segment(6) == '') ? 'A' : strtoupper($this->uri->segment(6));
            } else {
                $konten['jenis'] = strtoupper($this->uri->segment(5));
            }
        } else {
            $konten['jenis'] = strtoupper($this->uri->segment(5));
        }
        $this->load->view('form/' . $this->uri->segment(3), $konten);
    }

    public function simpanData()
    {
        $arrdata = array();
        $cid = '';
        $tabel = 'm_' . $this->uri->segment(3);
        foreach ($this->input->post() as $key => $value) {
            if (is_array($value)) { } else {
                $subject = strtolower($key);
                $pattern = '/tgl/i';
                if ($key == 'cid') {
                    $cid = $value;
                } else {
                    if (preg_match($pattern, $subject, $matches, PREG_OFFSET_CAPTURE)) {
                        if (strlen(trim($value)) > 0) {
                            $tgl = explode("/", $value);
                            $newtgl = $tgl[1] . "/" . $tgl[0] . "/" . $tgl[2];
                            $time = strtotime($newtgl);
                            $arrdata[$key] = date('Y-m-d', $time);
                        } else {
                            $arrdata[$key] = null;
                        }
                    } else {
                        if ($this->uri->segment(3) == "user") {
                            $arrdata['password'] = md5($this->input->post('password'));
                            $arrdata[$key] = $value;
                        }
                        $arrdata[$key] = $value;
                    }
                }
            }
        }
        if ($cid == "") {
            try {
                $this->Data_model->simpanData($arrdata, $tabel);
            } catch (Exception $exc) {
                echo $exc->getMessage();
            }
        } else {
            $kondisi = 'kode';
            try {
                $kondisi = array('kode' => $cid);
                echo $this->Data_model->updateDataWhere($arrdata, $tabel, $kondisi);
            } catch (Exception $exc) {
                echo $exc->getMessage();
            }
        }
    }

    public function hapus()
    {
        if (IS_AJAX) {
            $param = $this->input->post('cid');
            if ($this->input->post('cid') != '') {
                if (strpos($param, '-')) {
                    $exp = explode("-", $param);
                    $kondisi = array('kode' => $exp[0], 'seri' => $exp[1]);
                } else {
                    $kondisi = array('kode' => $param);
                }
                $this->Data_model->hapusDataWhere('m_' . $this->input->post('cod'), $kondisi);
                echo json_encode("ok");
            }
        }
    }

    function getKabupaten()
    {
        $kabupaten = $this->Data_model->ambilDataWhere('m_kota', array('kode_provinsi' => $_GET['kode']), 'nama', 'asc');
        echo '<select class="select2 form-control kota" name="kota" id="kota" onchange="loadKecamatan()">' .
            '<option value="">- Pilihan -</option>';
        foreach ($kabupaten as $row) {
            echo '<option data-id="' . $row->nama . '" value="' . $row->kode . '">' . $row->nama . '</option>';
        }
        echo '</select>';
    }

    function getKecamatan()
    {
        $kabupaten = $this->Data_model->ambilDataWhere('m_kecamatan', array('kode_kota' => $_GET['kode']), 'nama', 'asc');
        echo '<select class="select2 form-control kecamatan" name="kecamatan" id="kecamatan">' .
            '<option value="">- Pilihan -</option>';
        foreach ($kabupaten as $row) {
            echo '<option data-id="' . $row->nama . '" value="' . $row->kode . '">' . $row->nama . '</option>';
        }
        echo '</select>';
    }

    function getAgen(){
        $agen = $this->Data_model->ambilDataWhere('m_agen', array('provinsi' => $_GET['kode']), 'nama_agen', 'asc');
        echo '<select class="select2 form-control agen" name="agen" id="agen">' .
            '<option value="">- Pilihan -</option>';
        foreach ($kabupaten as $row) {
            echo '<option data-id="' . $row->nama_agen . '" value="' . $row->kode . '">' . $row->nama_agen . '</option>';
        }
        echo '</select>';
    }
}
