<?php if ($this->uri->segment(2) == '') {
    echo '<h1 style="text-align:center;">WALLBOARD DELIVERY SERVICE PERTAMINA</h1><br>';
} else if ($this->uri->segment(2) == 'index2') { 
    echo '<h3 style="text-align:center;">WALLBOARD PERTAMINA</h3><br>';
} else { ?>
    <nav class="navbar navbar-expand navbar-light bg-white topbar mb-4 static-top shadow">
        <?php if ($_SESSION['role'] == '4') : ?>
            <img src="<?= base_url('publik/logo/pertamina.png') ?>" style="width: 180px;height: 50px;" />
        <?php endif; ?>
        <!-- Sidebar Toggle (Topbar) -->
        <button id="sidebarToggleTop" class="btn btn-link d-md-none rounded-circle mr-3">
            <i class="fa fa-bars"></i>
        </button>

        <!-- Topbar Navbar -->
        <ul class="navbar-nav ml-auto">
            <img src="<?= base_url('publik/logo/infomedia.png') ?>" style="width: 70px;height: 65px;" />
            <div class="topbar-divider d-none d-sm-block"></div>

            <!-- Nav Item - User Information -->
            <li class="nav-item dropdown no-arrow">
                <a class="nav-link dropdown-toggle" href="#" id="userDropdown" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                    <span class="mr-2 d-none d-lg-inline text-gray-600 small"><?= $_SESSION['nama_lengkap'] ?></span>
                    <img class="img-profile rounded-circle" src="<?= base_url('publik/img/avatar.png') ?>">
                </a>
                <!-- Dropdown - User Information -->
                <div class="dropdown-menu dropdown-menu-right shadow animated--grow-in" aria-labelledby="userDropdown">
                    <a class="dropdown-item" href="<?= base_url('login/doOut') ?>" data-toggle="modal" data-target="#logoutModal">
                        <i class="fas fa-sign-out-alt fa-sm fa-fw mr-2 text-gray-400"></i>
                        Logout
                    </a>
                </div>
            </li>

        </ul>

    </nav>
<?php } ?>